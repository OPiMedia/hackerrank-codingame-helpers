#!/bin/bash

if [[ -z "$OUTPUT_PATH" ]]; then
    cat -
else
    cat - > "$OUTPUT_PATH"
fi
