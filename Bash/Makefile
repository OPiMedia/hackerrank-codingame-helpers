# Static analysis and cleaning (for Bash language) --- December 3, 2023

SRC = solution.sh



.SUFFIXES:

###########
# Options #
###########
DOS2UNIX = dos2unix
ECHO     = echo
HEAD     = head
LS       = ls
READ     = read
RM       = rm --force
RMDIR    = rmdir
SHELL    = bash
TEE      = tee
UNIX2DOS = unix2dos

FINISHED      = finished.sh  # https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/
FINISHEDFLAGS =

MAKE_HELP      = ./make_help  # https://bitbucket.org/OPiMedia/make_help/
MAKE_HELPFLAGS = --color


SHELLCHECK      = shellcheck  # https://www.shellcheck.net/
SHELLCHECKFLAGS =



###
# #
###
.PHONY:	all dos2unix finished unix2dos

all:	lintlog  # first target, runs lintlog target

dos2unix:  # convert data files to Unix end of lines
	$(DOS2UNIX) data/correct/*.txt
	$(DOS2UNIX) data/input/*.txt

finished:
	$(FINISHED) $(FINISHEDFLAGS)

unix2dos:  # convert data files to DOS end of lines
	$(UNIX2DOS) data/correct/*.txt
	$(UNIX2DOS) data/input/*.txt



###################
# Static analysis #
###################
.PHONY:	shellcheck lint lintlog

lint:	shellcheck  # runs each static analyser

lintlog:	# runs each static analyser and save to lint.log file
	@$(ECHO) 'Lint ('`date`') of solution.sh' | $(TEE) lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== `$(SHELLCHECK) $(SHELLCHECKFLAGS) --version | $(HEAD) --lines 1` ===== | $(TEE) --append lint.log
	-$(SHELLCHECK) $(SHELLCHECKFLAGS) $(SRC) | $(TEE) --append lint.log


shellcheck:	# runs static analyser ShellCheck
	@$(ECHO) ===== `$(SHELLCHECK) $(SHELLCHECKFLAGS) --version | $(HEAD) --lines 1` =====
	-$(SHELLCHECK) $(SHELLCHECKFLAGS) $(SRC)
	@$(ECHO)



#########
# Clean #
#########
.PHONY:	clean cleanData cleanLint cleanResult distclean forceCleanData keepFileEmptyDirectory overclean

clean:	cleanResult
	@if [ -z $(KEEPFILEEMPTYDIRECTORY) ]; then $(RM) data/correct/PUT_CORRECT_OUTPUT_FILES_HERE data/input/PUT_INPUT_FILES_HERE; fi

cleanData:	# asks confirmation and then deletes data files
	-@$(LS) -l data/correct/*.txt data/input/*.txt 2>/dev/null
	@if [ -z $(FORCECLEANDATA) ]; then $(READ) -p 'Delete "data" directory? [Y/n]' answer; if [ "$$answer" != 'Y' ]; then exit 1; fi; fi
	-$(RM) data/correct/*.txt
	-$(RM) data/input/*.txt

cleanLint:
	$(RM) lint.log

cleanResult:
	-$(RM) result/*.txt
	-$(RMDIR) result

distclean:	clean

forceCleanData:	# configures cleanData target to not ask confirmation
	$(eval FORCECLEANDATA = true)

keepFileEmptyDirectory:	# configures clean target to keep special files in data directories
	$(eval KEEPFILEEMPTYDIRECTORY = true)

overclean:	distclean cleanLint cleanData



########
# Help #
########
.PHONY:	h help

h:  # display only targets with description
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc ''

help:   # display all targets
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc '  %f\n'
