# Build and cleaning (for Pascal language) --- December 3, 2023

PROGS = solution

ALL_PROGS = $(PROGS)

SRCS = $(sort $(wildcard *.pas))
ALL_SRCS = $(SRCS)



.SUFFIXES:
.SUFFIXES:	.o

###########
# Options #
###########
DOS2UNIX = dos2unix
ECHO     = echo
LS       = ls
READ     = read
RM       = rm --force
RMDIR    = rmdir
SHELL    = bash
UNIX2DOS = unix2dos

FINISHED      = finished.sh  # https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/
FINISHEDFLAGS =

MAKE_HELP      = ./make_help  # https://bitbucket.org/OPiMedia/make_help/
MAKE_HELPFLAGS = --color


PASCAL      = fpc  # https://www.freepascal.org/
PASCALFLAGS = -vw -g -Sa


###
# #
###
.PHONY:	all dos2unix finished unix2dos

all:	$(PROGS)  # first target, compiles each program

dos2unix:  # convert data files to Unix end of lines
	$(DOS2UNIX) data/correct/*.txt
	$(DOS2UNIX) data/input/*.txt

finished:
	$(FINISHED) $(FINISHEDFLAGS)

unix2dos:  # convert data files to Unix end of lines
	$(UNIX2DOS) data/correct/*.txt
	$(UNIX2DOS) data/input/*.txt



########
# Rule #
########
%:	%.pas
	$(PASCAL) $(CPPFLAGS) $(PASCALFLAGS) $< -o$@



#########
# Clean #
#########
.PHONY:	clean cleanData cleanResult distclean forceCleanData keepFileEmptyDirectory overclean

clean:	cleanResult
	$(RM) *.o
	@if [ -z $(KEEPFILEEMPTYDIRECTORY) ]; then $(RM) data/correct/PUT_CORRECT_OUTPUT_FILES_HERE data/input/PUT_INPUT_FILES_HERE; fi

cleanData:	# asks confirmation and then deletes data files
	-@$(LS) -l data/correct/*.txt data/input/*.txt 2>/dev/null
	@if [ -z $(FORCECLEANDATA) ]; then $(READ) -p 'Delete "data" directory? [Y/n]' answer; if [ "$$answer" != 'Y' ]; then exit 1; fi; fi
	-$(RM) data/correct/*.txt
	-$(RM) data/input/*.txt

cleanResult:
	-$(RM) result/*.txt
	-$(RMDIR) result

distclean:	clean
	$(RM) $(PROGS)

forceCleanData:	# configures cleanData target to not ask confirmation
	$(eval FORCECLEANDATA = true)

keepFileEmptyDirectory:	# configures clean target to keep special files in data directories
	$(eval KEEPFILEEMPTYDIRECTORY = true)

overclean:	distclean cleanData



########
# Help #
########
.PHONY:	h help

h:  # display only targets with description
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc ''

help:   # display all targets
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc '  %f\n'
