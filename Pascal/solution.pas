program Solution;
{$H+}

uses sysutils;

var
   fout: TextFile;
   line : string;
begin
   assign(fout, GetEnvironmentVariable('OUTPUT_PATH'));
   rewrite(fout);

   while not eof() do
   begin
      readln(line);
      writeln(fout, line);
   end;

   close(fout);
   flush(StdErr);
   flush(output);
end.
