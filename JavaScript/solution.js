/* -*- coding: utf-8 -*- */

/* jshint esversion: 7 */
/* jshint node: true */
/* jshint strict: true */
/* jshint eqeqeq: true */

"use strict";


const fs = require("fs");


process.stdin.resume();
process.stdin.setEncoding("utf-8");


const lines = [];

process.stdin.on("data", text => {
    for (let line of text.split("\n")) {
        lines.push(line);
    }
});

process.stdin.on("end", () => {
    if (lines[lines.length - 1] === "") lines.pop();
    main();
});



function main() {
    const fOut = fs.createWriteStream(process.env.OUTPUT_PATH);

    for (let line of lines) {
        fOut.write(line + "\n");
    }

    fOut.end();
}
