# Config file for run.sh and foreach.sh (for solution program in SQL)
# See *List of configuration variables* in
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/

# INPUT_PATTERN='data/input/input*.txt'    # pattern of input files
# OUTPUT_PATTERN='data/correct/output*.txt'  # pattern of corresponding correct output files


VM='./sqlite/sqlite_run.sh'  # virtual machine, to run PROG
VM_FLAGS=('result/dbs/data*.db' './solution.sql')


# ANALYZER=''  # dynamic analyzer, enabled by -z or -Z options
# ANALYZER_FLAGS=()


PROG='./solution.sql'  # main program
PROG_FLAGS=()

PROG_SRC='solution.sql'  # source code


INPUT_HEAD=0   # max number of lines to print, for input file
# RESULT_HEAD=0  # max number of lines to print, for result file
# OUTPUT_HEAD=0  # max number of lines to print, for output file


MAKE_RUN=0  # if 0 then run make step


# DIFF='sdiff'      # side by side differences
# DIFF='colordiff'  # color diff
# DIFF_FLAGS=()


# Predefined others differences viewer, enabled by -v or -V options
## KDiff3
# DIFF_VIEW='kdiff3'
# DIFF_VIEW_FLAGS=()
# DIFF_VIEW_PARAM_RESULT_OUTPUT=''

## Emacs
# DIFF_VIEW='emacs'
# DIFF_VIEW_FLAGS=('--eval')
# DIFF_VIEW_PARAM_RESULT_OUTPUT='(ediff-files "%r" "%o")'

## Vim
# DIFF_VIEW='vim'
# DIFF_VIEW_FLAGS=('-d')
# DIFF_VIEW_PARAM_RESULT_OUTPUT=''
