#!/bin/bash

#
# Usage: sqlite_create_dbs.sh [--list] [DB_FILENAME]
#
# For each COMMON found by "foreach.sh",
# create a DB file in "result/dbs/"
# with the same "data/tables.sql"
# and each data specified by corresponding input files.
#
#
# Latest version of "HackerRank [CodinGame...] / helpers",
# config files for several programming languages,
# and more explanation on Bitbucket:
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/
#
# GPLv3 --- Copyright (C) 2020, 2021 Olivier Pirson
# http://www.opimedia.be/
#
# * January 1st, 2021.
# * Started December 31, 2020.
##

if [[ "$1" == '--list' ]]; then
    LIST=0
    ONLY_DB_FILE="$2"
else
    LIST=1
    ONLY_DB_FILE="$1"
fi


CREATE_TABLES='data/tables.sql'

SQL='sqlite3'
declare -a SQL_FLAGS=()


# Get COMMON parts and associated input filenames
declare -a COMMONS=($(./foreach.sh -l | tail -n +2 | sed -E 's/"\s+"/"\t"/g' | cut -f1))
declare -a INPUTS=($(./foreach.sh -l | tail -n +2 | sed -E 's/"\s+"/"\t"/g' | cut -f2))


# Create results directory
if [[ "${LIST}" -ne 0 ]]; then
    mkdir -p result/dbs
fi


# For each COMMON part
for I in "${!COMMONS[@]}"; do
    COMMON="${COMMONS[${I}]//\"/}"
    INPUT="${INPUTS[${I}]//\"/}"
    DB_FILE="result/dbs/data${COMMON}.db"

    if [[ -z "${ONLY_DB_FILE}" || "${DB_FILE}" == "${ONLY_DB_FILE}" ]]; then
        if [[ "${LIST}" -eq 0 ]]; then
            # Only print data filenames
            while IFS=' ' read -r NAME FILE; do
                if [[ "${NAME}" == '#'* || -z "${NAME}" || -z "${FILE}" ]]; then continue; fi
                echo "${FILE}"
            done <<< "$(cat "${INPUT}")"
        else
            # Create DB file with tables and fill them from data files
            echo "\"${COMMON}\""
            rm -f "${DB_FILE}"

            echo "  Create database in \"${INPUT}\""
            # shellcheck disable=SC2086
            "${SQL}" ${SQL_FLAGS[*]} "${DB_FILE}" ".read ${CREATE_TABLES}"

            while IFS=' ' read -r NAME FILE; do
                if [[ "${NAME}" == '#'* || -z "${NAME}" || -z "${FILE}" ]]; then continue; fi
                echo "  Import data from \"${FILE}\" to table \"${NAME}\""
                # shellcheck disable=SC2086
                "${SQL}" ${SQL_FLAGS[*]} "${DB_FILE}" -cmd ".separator ' '" ".import ${FILE} ${NAME}"
            done <<< "$(cat "${INPUT}")"
        fi
    fi
done
