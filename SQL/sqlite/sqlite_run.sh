#!/bin/bash

#
# Usage: sqlite_run.sh DB_FILENAME PROG_SQL
#
# Run PROG_SQL on the DB DB_FILENAME.
#
#
# Latest version of "HackerRank [CodinGame...] / helpers",
# config files for several programming languages,
# and more explanation on Bitbucket:
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/
#
# GPLv3 --- Copyright (C) 2020, 2021 Olivier Pirson
# http://www.opimedia.be/
#
# * January 1st, 2021.
# * Started December 31, 2020.
##

sqlite3 "$1" ".read $2"
