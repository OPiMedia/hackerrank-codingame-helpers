#!/bin/bash

#
# Usage: run.sh [options] [COMMON]
#
# Run your solution program on given input and compare result.
#
# 0) If option -m, run "make" to compile when it is necessary.
# 1) Display "data/input/inputCOMMON.txt" input file.
# 2) Run solution program with "data/input/inputCOMMON.txt" in standard input.
# 3) Compare "result/result.txt" result file
#    with "data/correct/outputCOMMON.txt" correct output file.
# 4) Display "result/result.txt" result file.
# 5) Display "data/correct/outputCOMMON.txt" correct output file.
#
# By default solution program is set by
#   PROG='./solution.py'    # main program
#   PROG_FLAGS=()           # eventual parameters
#   PROG_SRC='solution.py'  # source code
#
# If PROG_SRC contents the string "OUTPUT_PATH"
# then solution program must use then environment variable OUTPUT_PATH
# and write to this file,
# else it must write to standard output.
#
# If file "config.sh" exists in the current working directory
# then it is loaded to override configuration variables.
#
# For example:
#   For run Python program with -O option, set in config file:
#     VM='python3'  # virtual machine, to run PROG
#     VM_FLAGS=('-O')
#     PROG='./solution.py'
#
#   For Java program, set:
#     VM='java'
#     PROG='Solution.class'  # (mention .class, it will be removed to run)
#     PROG_SRC='Solution.java'
#     MAKE_RUN=0  # to run make automatically in step 0 (even without -m option)
#
#   For C++ program, set:
#     PROG='./solution'
#     PROG_SRC='solution.cpp'
#     MAKE_RUN=0
#
#   For match different patterns of data filename:
#     INPUT_PATTERN='data/zzz*'
#     OUTPUT_PATTERN='data/*/*.txt'
#     Associates filename 'data/zzzCOMMON' to 'data/COMMON/COMMON.txt',
#     for any COMMON string (even empty).
#
# Options:
#   -e   compare last end of line (else ignore it)
#   -m   run "make" before other steps
#   -t   pure text, disable ANSI colors
#   -v   launch difference viewer (like meld, kdiff3...) specified by DIFF_VIEW
#          when found difference between result and output files
#   -z   dynamic analyzer (like valgrind) specified by ANALYZER
#
#   --   mark the end of foreach.sh options, remain is considered as COMMON
#
#   --help     display this help message and quit
#   --version  display version information and quit
#
# Exit status:
#   * 0 if all is OK and result and correct output files are equal
#   * 100 if fails in step 0) building by make
#   * 101 if fails in step 1) display of input file
#   * 102 if fails in step 2) run of solution program
#   * 103 if fails in step 3) compare result with correct output files,
#         or if these files are different
#   * 104 if fails in step 4) display of result file
#   * 105 if fails in step 5) display of correct output file
#
#
# Requirements:
# * cat, cmp, grep, head, mkdir, tee, time
# * make for option -m
# * meld for option -v
# * valgrind for option -z
#
#
# Latest version of "HackerRank [CodinGame...] / helpers",
# config files for several programming languages,
# and more explanation on Bitbucket:
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/
#
# GPLv3 --- Copyright (C) 2020-2021, 2023, 2025 Olivier Pirson
# http://www.opimedia.be/
##
# * January 7, 2025: Corrected typo.
# * December 3, 2023:
#   - Added DIFF_VIEW_PARAM_RESULT_OUTPUT configuration variable (to start Emacs Ediff).
#   - Replaced bad use of [*] by [@].
# * December 2, 2023: Few cleaning.
# * December 8, 2021: Corrected bug about IFS expansion.
# * November 14, 2021: Cleaned IFS.
# * October 29, 2021: Cleaned Bash style.
# * January 1st, 2021:
#   - Added * substitution by COMMON in executed command.
#   - Cleaned variables invocations.
# * December 6, 2020: Replaced default value 1 of *_HEAD by 3.
# * December 5, 2020:
#   - Replaced special characters %s in patterns by the single *.
#   - Replaced wrong postfix name by common.
# * September 8, 2020:
#   - Split data/ directory in data/input and data/correct.
# * August 24, 2020:
#   - Added mark to extract list of configuration variables in README.
#   - Added *_RUN configuration variables to enable/disable each step.
# * August 19, 2020.
# * Started August 9, 2020.
#

#
# Configuration variables
#
# <<< begin of configuration variables
INPUT_PATTERN='data/input/input*.txt'    # pattern of input files
OUTPUT_PATTERN='data/correct/output*.txt'  # pattern of corresponding correct output files

TIME='time'  # virtual machine, to run PROG (internal Bash command)
declare -ag TIME_FLAGS=('-p')

ANALYZER=''  # dynamic analyzer, enabled by -z option
declare -ag ANALYZER_FLAGS=()

declare -ig ANALYZER_RUN=1  # if 0 then run dynamic analyzer

VM=''  # virtual machine, to run PROG
declare -ag VM_FLAGS=()

PROG='./solution.py'  # main program
declare -ag PROG_FLAGS=()

PROG_SRC='solution.py'  # source code

MAKE='make'  # command use to compile
declare -ag MAKE_FLAGS=()

RESULT_PATTERN='result/result*.txt'  # pattern of result files

CMP='cmp'  # command used to compare
declare -ag CMP_FLAGS=()

declare -ig CMP_LAST_EMPTY_LINE=1  # if 0 then compare also the eventual last empty line

DIFF='diff'  # command display difference when different
declare -ag DIFF_FLAGS=()

DIFF_VIEW='meld'    # differences viewer, enabled by -v option
declare -ag DIFF_VIEW_FLAGS=('--newtab')
DIFF_VIEW_PARAM_RESULT_OUTPUT=''  # if empty then adds result and output files, else only adds DIFF_VIEW_PARAM_RESULT_OUTPUT by replacing "%r" and "%o" by these files

declare -ig DIFF_VIEW_PRINT=0  # if 0 then print command to launch DIFF_VIEW when different
declare -ig DIFF_VIEW_RUN=1  # if 0 then run difference viewer when result and output are different

# Set '' to print all content, or 0 to no print
INPUT_HEAD=3   # max number of lines to print, for input file
RESULT_HEAD=3  # max number of lines to print, for result file
OUTPUT_HEAD=3  # max number of lines to print, for output file

declare -ig MAKE_RUN=1    # if 0 then run step 0) building by make
declare -ig INPUT_RUN=0   # if 0 then run step 1) display of input file
declare -ig PROG_RUN=0    # if 0 then run step 2) run of solution program
declare -ig CMP_RUN=0     # if 0 then run step 3) compare result with correct output files
declare -ig RESULT_RUN=0  # if 0 then run step 4) display of result file
declare -ig OUTPUT_RUN=0  # if 0 then run step 5) display of correct output file


E_NORMAL='\e[0m'
E_MAGENTA='\e[95m'
E_BG_RED='\e[101m'
# end of configuration variables >>>


CONFIG='config.sh'

if [[ -f "${CONFIG}" ]]; then
    # shellcheck disable=SC1090
    source "${CONFIG}"
fi



#
# Functions
#

# Print content of $1 file.
# If $2 is specified
# then only print $2 first lines.
#      In this case, if there is more lines then print ... and number of lines.
function cat_head {
    local -r filename=$1
    declare -ir n=$2

    if [[ -f "${filename}" ]]; then
        if [[ -s "${filename}" ]]; then
            if [[ -z "$2" ]]; then
                cat "${filename}"
            else
                if [[ "${n}" -ne 0 ]]; then
                    declare -i length

                    length=$(wc --lines < "${filename}")
                    head --lines "${n}" < "${filename}"
                    if [[ "${length}" -gt "${n}" ]]; then
                        echo "  [... ${n}/${length}]"
                    fi
                fi
            fi
        else
            if [[ "${n}" -ne 0 ]]; then
                printf '[empty file "%s"]\n' "${filename}"
            fi
        fi
    else
        printf '  '
        print_colored '[File "'"${filename}"'" doesn'"'"'t exist!]'
        echo

        return 1
    fi
}


# If file $1 doesn't exist
# then print error message and quit.
function check_exist {
    local -r filename=$1

    if [[ ! -f "${filename}" ]]; then
        print_colored ' File "'"${filename}"'" doesn'"'"'t exist! '
        echo

        return 1
    fi
}


# Return 0 if file $1 exists and ending by end of line character,
# else return 1
function check_ending_eol {
    local -r filename=$1

    [[ -s "${filename}" && -z "$(tail -c 1 "${filename}")" ]]
}


# Check if all patterns contain the right number of wildcard *.
# If not print the help message and exit 1.
function check_patterns {
    local pattern
    local removed

    for name_pattern in INPUT_PATTERN OUTPUT_PATTERN RESULT_PATTERN; do
        pattern="${!name_pattern}"
        removed=$(wildcard_substitute "${pattern}")
        if [[ "${name_pattern}" == 'INPUT_PATTERN' ]]; then
            if [[ $((${#pattern} - ${#removed})) -ne 1 ]]; then
                echo "!${name_pattern} must contain exactly one wildcard *"
                print_help

                exit 1
            fi
        else
            if [[ $((${#pattern} - ${#removed})) -lt 1 ]]; then
                echo "!${name_pattern} must contain at least one wildcard *"
                print_help

                exit 1
            fi
        fi
    done
}


## Display the command with parameters protected by quotes if necessary.
function display_complete_command {  # command [parameter...]
    local -r command=$1
    shift
    printf '%s' "${command}"

    local param
    for param in "$@"; do
        if [[ "${param}" != '&' ]] && grep --ignore-case --no-messages --quiet '[^-./0-9A-Z_]' <<< "${param}"; then
            # Need to be protected
            param=$(declare -p param)
            param=${param#declare -- param=}
        fi
        printf ' %s' "${param}"
    done
    echo
}


# Print text $1.
# If $2 then print it on red background.
function print_colored {
    local -r text=$1
    local -r error=$2

    # shellcheck disable=SC2059
    if [[ "${error}" -eq 0 ]]; then printf "${E_BG_RED}"; fi

    printf '%s' "${text}"

    # shellcheck disable=SC2059
    if [[ "${error}" -eq 0 ]]; then printf "${E_NORMAL}"; fi
}


# Print text $1 (without right spaces) in magenta.
function print_colored_title {
    [[ "$1" =~ .*[^[:space:]] ]]
    printf "${E_MAGENTA}"'========== %s =========='"${E_NORMAL}"'\n' "${BASH_REMATCH[0]}"
}


# Print help message (read from the script itself).
function print_help {
    local line
    local -i skip=0

    while IFS='' read -r line; do
        if [[ "${line}" == '# Usage:'* ]]; then skip=1; fi
        if [[ "${skip}" -eq 0 ]]; then continue; fi
        if [[ "${line}" == '##'* ]]; then break; fi
        echo "${line:2}"
    done < "${BASH_SOURCE[0]}";

    echo "Current version: $(print_version)"
}


# Prints version (reads from the script itself)
function print_version {
    local line
    local -i skip_begin=0
    local version

    while IFS='' read -r line; do
        if [[ "$line" == '##'* ]]; then skip_begin=1; fi
        if [[ "$skip_begin" -eq 0 ]]; then continue; fi
        line="${line:4}"
        if [[ -n "$line" ]]; then
            IFS=':' read -r version line <<< "$line"
            echo "$version"

            break
        fi
    done < "${BASH_SOURCE[0]}"
}


# Print URL of the file $1.
function url {
    echo "file://${PWD}/$1"
}


# Return $1 with each * character substituted by $2.
function wildcard_substitute {
    echo "${1//\*/$2}"
}



# Set options from $@
# and set COMMON.
function main__set_options {
    declare -i found=0

    while [[ "${found}" -eq 0 ]]; do
        case "$1" in
            --help)
                print_help

                exit 0
                ;;
            -e)  # compare last end of line
                CMP_LAST_EMPTY_LINE=0
                ;;
            -m)  # run "make" in step 0
                MAKE_RUN=0
                ;;
            -t)  # disable ANSI colors
                E_NORMAL=''
                E_MAGENTA=''
                E_BG_RED=''
                ;;
            -v)  # launch difference viewer
                DIFF_VIEW_RUN=0
                ;;
            --version)
                echo "run.sh ($(print_version))"

                exit 0
                ;;
            -z)  # run solution program with dynamic analyzer
                ANALYZER_RUN=0
                ;;
            --)  # end of options
                shift

                break
                ;;
            *)
                found=1
                ;;
        esac
        if [[ "${found}" -eq 0 ]]; then
            shift
        fi
    done

    COMMON="$1"
}


# Step 0) Run "make".
function main__step0_make {
    print_colored_title "0) compile: $(display_complete_command "${MAKE}" "${MAKE_FLAGS[@]}")"
    check_exist "${PROG_SRC}" || exit 100

    "${MAKE}" "${MAKE_FLAGS[@]}" || exit 100
}


# Step 1) Display input file.
function main__step1_display_input {
    print_colored_title "1) input \"${INPUT}\" <$(url "${INPUT}")>"
    check_exist "${INPUT}" || exit 101

    cat_head "${INPUT}" "${INPUT_HEAD}" || exit 101
}


# Step 2) Run solution program.
# If PROG_SRC contains "OUTPUT_PATH"
# then
function main__step2_run_prog {
    # Identify output (stdout or file)
    grep 'OUTPUT_PATH' "${PROG_SRC}" &> /dev/null && export OUTPUT_PATH="${RESULT}"
    if [[ -z "${OUTPUT_PATH}" ]]; then
        MSG_OUTPUT="(stdout redirected to \"${RESULT}\")"
    else
        MSG_OUTPUT="(write to \"${OUTPUT_PATH}\", via OUTPUT_PATH)"
    fi

    declare -a params

    # time
    if [[ -n "${TIME}" ]]; then
        params+=("${TIME}")
        if [[ "${#TIME_FLAGS[@]}" -ne 0 ]]; then params+=("${TIME_FLAGS[@]}"); fi
    fi

    # Analyzer
    if [[ "${ANALYZER_RUN}" -eq 0 && -n "${ANALYZER}" ]]; then
        params+=("${ANALYZER}")
        if [[ "${#ANALYZER_FLAGS[@]}" -ne 0 ]]; then params+=("${ANALYZER_FLAGS[@]}"); fi
    fi

    # Virtual machine
    if [[ -n "${VM}" ]]; then
        params+=("${VM}")
        if [[ "${#VM_FLAGS[@]}" -ne 0 ]]; then params+=("${VM_FLAGS[@]}"); fi
    fi

    # Solution program
    if [[ -n "${PROG}" ]]; then
        if [[ "${PROG}" == *'.class' ]]; then
            params+=("${PROG::-6}")
        else
            params+=("${PROG}")
        fi
        if [[ "${#PROG_FLAGS[@]}" -ne 0 ]]; then params+=("${PROG_FLAGS[@]}"); fi
    fi

    # Substitute * by value COMMON
    for i in "${!params[@]}"; do
        params[$i]=$(wildcard_substitute "${params[$i]}" "${COMMON}")
    done

    # Print title
    print_colored_title "2) run: $(display_complete_command "${params[@]}") ${MSG_OUTPUT}"
    check_exist "${PROG}" || exit 102

    # Execute
    if [[ -z "${OUTPUT_PATH}" ]]; then  # write to standard output
        "${params[@]}" < "${INPUT}" | tee "${RESULT}" || exit 102
        if [[ "${PIPESTATUS[0]}" -ne 0 ]]; then
            exit 102
        fi
    else                                # solution program write itself to ${RESULT} file
        "${params[@]}" < "${INPUT}" || exit 102
    fi
}


# Step 3) Compare result with correct output files
# and return exit code of comparison.
function main__step3_compare {
    check_exist "${RESULT}" || exit 103
    check_exist "${OUTPUT}" || exit 103

    local -i end_result=0
    local -i end_output=0
    local -i code

    if [[ "${CMP_LAST_EMPTY_LINE}" -ne 0 ]]; then
        check_ending_eol "${RESULT}"
        end_result=$?
        check_ending_eol "${OUTPUT}"
        end_output=$?
    fi

    if [[ (("${end_result}" -eq 0) && ("${end_output}" -eq 0)) ||
              (("${end_result}" -ne 0) && ("${end_output}" -ne 0)) ]]; then
        # Both result and output files finished by empty line or not
        print_colored_title "3) compare with: $(display_complete_command "${CMP}" "${CMP_FLAGS[@]}" "${RESULT}" "${OUTPUT}")"
        "${CMP}" "${CMP_FLAGS[@]}" "${RESULT}" "${OUTPUT}"
    else
        # One of result and output files finished by empty line and the other not,
        # avoid it to compare
        print_colored_title "3) compare with (ending files different): $(display_complete_command "${CMP}" "${CMP_FLAGS[@]}" "${RESULT}" "${OUTPUT}")"
        if [[ "${end_result}" -eq 0 ]]; then
            # Final eol in result but not in output
            head --bytes -1 "${RESULT}" | "${CMP}" "${CMP_FLAGS[@]}" - "${OUTPUT}"
        else
            # Final eol in out but not in result
            head --bytes -1 "${OUTPUT}" | "${CMP}" "${CMP_FLAGS[@]}" "${RESULT}" -
        fi
    fi

    case "$?" in
        0)  # identical files (or last empty line is the only difference)
            code=0
            ;;
        1)  # different files
            print_colored ' DIFFERENT '
            echo
            code=103
            if [[ -n "${DIFF}" ]]; then  # display difference
                "${DIFF}" "${DIFF_FLAGS[@]}" "${RESULT}" "${OUTPUT}"
            fi

            local -a complete_diff_view=("${DIFF_VIEW}")

            complete_diff_view+=("${DIFF_VIEW_FLAGS[@]}")

            if [[ -z "${DIFF_VIEW_PARAM_RESULT_OUTPUT}" ]]; then
                complete_diff_view+=("${RESULT}" "${OUTPUT}")
            else
                local tmp="${DIFF_VIEW_PARAM_RESULT_OUTPUT//%r/${RESULT}}"

                complete_diff_view+=("${tmp//%o/${OUTPUT}}")
            fi

            if [[ "${DIFF_VIEW_PRINT}" -eq 0 ]]; then  # display differences viewer command
                if [[ "${DIFF_VIEW}" == 'vi' || "${DIFF_VIEW}" == 'vim' ]]; then
                    display_complete_command "${complete_diff_view[@]}"
                else
                    display_complete_command "${complete_diff_view[@]}" '&'
                fi
            fi
            if [[ "${DIFF_VIEW_RUN}" -eq 0 ]]; then  # launch differences viewer command
                if [[ "${DIFF_VIEW}" == 'vi' || "${DIFF_VIEW}" == 'vim' ]]; then
                    "${complete_diff_view[@]}"
                else
                    "${complete_diff_view[@]}" &
                fi
            fi
            ;;
        *)  # comparison error
            exit 103
            ;;
    esac

    return "${code}"
}


# Step 4) Display result file.
function main__step4_display_result {
    print_colored_title "4) result \"${RESULT}\" <$(url "${RESULT}")>"

    cat_head "${RESULT}" "${RESULT_HEAD}" || exit 104
}


# Step 5) Display correct output file.
function main__step5_display_correct_output {
    print_colored_title "5) correct output \"${OUTPUT}\" <$(url "${OUTPUT}")>"

    cat_head "${OUTPUT}" "${OUTPUT_HEAD}" || exit 105
}



#
# Main
#

# Set (and remove) options
COMMON=

check_patterns

main__set_options "$@"


INPUT=$(wildcard_substitute "${INPUT_PATTERN}" "${COMMON}")
OUTPUT=$(wildcard_substitute "${OUTPUT_PATTERN}" "${COMMON}")
RESULT=$(wildcard_substitute "${RESULT_PATTERN}" "${COMMON}")

mkdir --parents "$(dirname "${RESULT}")"



# Run each step

declare -i CMP_CODE=0

if [[ "${MAKE_RUN}" -eq 0 ]]; then
    main__step0_make
    echo
fi

if [[ "${INPUT_RUN}" -eq 0 ]]; then
    main__step1_display_input
    echo
fi

if [[ "${PROG_RUN}" -eq 0 ]]; then
    main__step2_run_prog
    echo
fi

if [[ "${CMP_RUN}" -eq 0 ]]; then
    main__step3_compare
    CMP_CODE="$?"
    echo
fi

if [[ "${RESULT_RUN}" -eq 0 ]]; then
    main__step4_display_result
    echo
fi

if [[ "${OUTPUT_RUN}" -eq 0 ]]; then
    main__step5_display_correct_output
fi


exit "${CMP_CODE}"
