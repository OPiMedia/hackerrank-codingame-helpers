# Build, static analysis and cleaning (for C++ language) --- December 3, 2023

PROGS = solution

SRCS_CPP = $(sort $(wildcard *.cpp))
ALL_SRCS_CPP = $(SRCS_CPP)

SRCS_HPP = $(sort $(wildcard *.hpp))
ALL_SRCS_HPP = $(SRCS_HPP)

ALL_SRCS = $(ALL_SRCS_HPP) $(ALL_SRCS_CPP)



.SUFFIXES:
.SUFFIXES:	.o

###########
# Options #
###########
DOS2UNIX = dos2unix
ECHO     = echo
GREP     = grep
HEAD     = head
LS       = ls
READ     = read
RM       = rm --force
RMDIR    = rmdir
SHELL    = bash
TAIL     = tail
TEE      = tee
UNIX2DOS = unix2dos

FINISHED      = finished.sh  # https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/
FINISHEDFLAGS =

MAKE_HELP      = ./make_help  # https://bitbucket.org/OPiMedia/make_help/
MAKE_HELPFLAGS = --color


CPP      = g++  # https://gcc.gnu.org/
CPPFLAGS =

CXX      = g++
CXXFLAGS = -std=c++17 -pedantic -Wall -Wextra -g

LD      = g++
LDFLAGS = -lm
LIBS    =


CLANGTIDY      = clang-tidy  # https://clang.llvm.org/extra/clang-tidy/
CLANGTIDYFLAGS =
CLANGTIDYAFTER = -- -I .

CPPCHECK      = cppcheck  # https://cppcheck.sourceforge.io/
CPPCHECKFLAGS = -I . --check-config --check-library --enable=all --platform=unix64 --report-progress --std=c++14 --suppress=missingIncludeSystem --verbose

CPPLINT      = cpplint  # https://pypi.org/project/cpplint
CPPLINTFLAGS = --root . --extension hpp,cpp --filter=-build/namespaces,-legal/copyright,-readability/braces,-readability/casting,-whitespace/newline,-whitespace/line_length

INFER      = infer  # https://fbinfer.com/
INFERFLAGS =

SCANBUILD      = scan-build  # https://clang-analyzer.llvm.org/
SCANBUILDFLAGS =



###
# #
###
.PHONY:	all dos2unix finished unix2dos

all:	$(PROGS)  # first target, compiles each program

dos2unix:  # convert data files to Unix end of lines
	$(DOS2UNIX) data/correct/*.txt
	$(DOS2UNIX) data/input/*.txt

finished:
	$(FINISHED) $(FINISHEDFLAGS)

unix2dos:  # convert data files to DOS end of lines
	$(UNIX2DOS) data/correct/*.txt
	$(UNIX2DOS) data/input/*.txt



#######################
# Compilation options #
#######################
.PHONY:	clang ndebug nhelper

clang:  # Use Clang compiler instead GCC: https://clang.llvm.org/
	$(eval CPP = clang++)
	$(eval CXX = clang++)
	$(eval LD = clang++)

ndebug:  # disable debug information
	$(eval CPPFLAGS += -DNDEBUG)
	$(eval CXXFLAGS += -O3)
	$(eval LDFLAGS += --strip-all)

nhelper:  # disable printing by helper.hpp
	$(eval CPPFLAGS += -DNHELPER)



#########
# Rules #
#########
.PRECIOUS:	%.o

%:	%.o
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

%.o:	%.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@



###################
# Static analysis #
###################
.PHONY:	clang-tidy cppcheck cpplint infer lint lintlog scan-build

clang-tidy:	# runs static analyser Clang-Tidy
	@$(ECHO) ===== Clang-Tidy `$(CLANGTIDY) $(CLANGTIDYFLAGS) --version | $(HEAD) --lines 2 | $(TAIL) --lines 1` =====
	-$(CLANGTIDY) $(CLANGTIDYFLAGS) $(ALL_SRCS) $(CLANGTIDYAFTER)
	@$(ECHO)

cppcheck:	# runs static analyser Cppcheck
	@$(ECHO) ===== `$(CPPCHECK) $(CPPCHECKFLAGS) --version` =====
	-$(CPPCHECK) $(CPPCHECKFLAGS) $(ALL_SRCS)
	@$(ECHO)

cpplint:	# runs static analyser cpplint
	@$(ECHO) ===== `$(CPPLINT) $(CPPLINTFLAGS) --version | $(TAIL) --lines -3` =====
	-$(CPPLINT) $(CPPLINTFLAGS) $(ALL_SRCS)
	@$(ECHO)

infer:	# runs static analyser Infer
	@$(ECHO) ===== `$(INFER) $(INFERFLAGS) --version | $(HEAD) --lines 1` =====
	-$(INFER) $(INFERFLAGS) run -- $(CXX) $(CPPFLAGS) $(CXXFLAGS) -fPIC -c $(ALL_SRCS)
	@$(ECHO)

scan-build:	# runs static analyser scan-build (Clang Static Analyzer)
	@$(ECHO) ===== 'scan-build (Clang Static Analyzer)' =====
	-$(SCANBUILD) $(SCANBUILDFLAGS) make 2>&1 | $(GREP) --invert-match 'scan-build: Removing directory .* because it contains no reports.'
	@$(ECHO)


lint:	clang-tidy cppcheck cpplint scan-build infer  # runs each static analyser

lintlog:	# runs each static analyser and save to lint.log file
	@$(ECHO) 'Lint ('`date`') of solution' | $(TEE) lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== Clang-Tidy `$(CLANGTIDY) $(CLANGTIDYFLAGS) --version | $(HEAD) --lines 2 | $(TAIL) --lines 1` ===== | $(TEE) --append lint.log
	-$(CLANGTIDY) $(CLANGTIDYFLAGS) $(ALL_SRCS) $(CLANGTIDYAFTER) 2>&1 | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== `$(CPPCHECK) $(CPPCHECKFLAGS) --version` ===== | $(TEE) --append lint.log
	-$(CPPCHECK) $(CPPCHECKFLAGS) $(ALL_SRCS) 2>&1 | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== `$(CPPLINT) $(CPPLINTFLAGS) --version | $(TAIL) --lines -3` ===== | $(TEE) --append lint.log
	-$(CPPLINT) $(CPPLINTFLAGS) $(ALL_SRCS) 2>&1 | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== 'scan-build (Clang Static Analyzer)' ===== | $(TEE) --append lint.log
	-$(SCANBUILD) $(SCANBUILDFLAGS) make 2>&1 | $(GREP) --invert-match 'scan-build: Removing directory .* because it contains no reports.' | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== `$(INFER) $(INFERFLAGS) --version | $(HEAD) --lines 1` ===== | $(TEE) --append lint.log
	-$(INFER) $(INFERFLAGS) run -- $(CXX) $(CPPFLAGS) $(CXXFLAGS) -fPIC -c $(ALL_SRCS) 2>&1 | $(TEE) --append lint.log



#########
# Clean #
#########
.PHONY:	clean cleanData cleanLint cleanResult distclean forceCleanData keepFileEmptyDirectory overclean

clean:	cleanResult
	$(RM) *.o core *.core gmon.out cachegrind.out.*
	$(RM) helper.hpp.gch
	$(RM) --recursive infer-out
	@if [ -z $(KEEPFILEEMPTYDIRECTORY) ]; then $(RM) data/correct/PUT_CORRECT_OUTPUT_FILES_HERE data/input/PUT_INPUT_FILES_HERE; fi

cleanData:	# asks confirmation and then deletes data files
	-@$(LS) -l data/correct/*.txt data/input/*.txt 2>/dev/null
	@if [ -z $(FORCECLEANDATA) ]; then $(READ) -p 'Delete "data" directory? [Y/n]' answer; if [ "$$answer" != 'Y' ]; then exit 1; fi; fi
	-$(RM) data/correct/*.txt
	-$(RM) data/input/*.txt

cleanLint:
	$(RM) lint.log

cleanResult:
	-$(RM) result/*.txt
	-$(RMDIR) result

distclean:	clean
	$(RM) $(PROGS)

forceCleanData:	# configures cleanData target to not ask confirmation
	$(eval FORCECLEANDATA = true)

keepFileEmptyDirectory:	# configures clean target to keep special files in data directories
	$(eval KEEPFILEEMPTYDIRECTORY = true)

overclean:	distclean cleanLint cleanData



########
# Help #
########
.PHONY:	h help

h:  # display only targets with description
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc ''

help:   # display all targets
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc '  %f\n'
