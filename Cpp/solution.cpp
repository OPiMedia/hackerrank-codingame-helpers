/* -*- coding: latin-1 -*- */
#if __has_include("helper.hpp")
#  include "helper.hpp"
#endif
#ifndef TSV
#  define PRINT_LIST(...)
#  define TSV(...)
#endif


// #include <bits/stdc++.h>

#include <fstream>
#include <iostream>
#include <string>

using namespace std;


int main() {
  const char* const filename = getenv("OUTPUT_PATH");

  if (filename == nullptr) return EXIT_FAILURE;

  ofstream fout(filename);
  string line;

  while (getline(cin, line)) {
    fout << line << endl;
  }

  fout.close();

  return EXIT_SUCCESS;
}
