/* -*- coding: latin-1 -*- */
/** \file helper.hpp (August 29, 2020)
 * \brief
 * Some generic helper macros.
 *
 * GPLv3 --- Copyright (C) 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef HELPER_HPP_
#define HELPER_HPP_



/* ****************
 * Private macros *
 ******************/

/** \brief
 * From "Variadic macros tricks"
 * https://codecraft.co/2014/11/25/variadic-macros-tricks/
 */
#define GET_OVERLOAD_(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, NAME, ...) NAME


#define TSV1_(a) {                              \
    std::cerr << (a) << std::endl;              \
    std::cerr.flush();                          \
  }

#define TSV2_(a, b) {                                   \
    std::cerr << (a) << '\t' << (b) << std::endl;       \
    std::cerr.flush();                                  \
  }

#define TSV3_(a, b, c) {                                                \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << std::endl;        \
    std::cerr.flush();                                                  \
  }

#define TSV4_(a, b, c, d) {                                             \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << std::endl; \
    std::cerr.flush();                                                  \
  }

#define TSV5_(a, b, c, d, e) {                                          \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << std::endl; \
    std::cerr.flush();                                                  \
  }

#define TSV6_(a, b, c, d, e, f) {                                       \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << std::endl;                                      \
    std::cerr.flush();                                                  \
  }

#define TSV7_(a, b, c, d, e, f, g) {                                    \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << std::endl;                       \
    std::cerr.flush();                                                  \
  }

#define TSV8_(a, b, c, d, e, f, g, h) {                                 \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << '\t' << (h) << std::endl;        \
    std::cerr.flush();                                                  \
  }

#define TSV9_(a, b, c, d, e, f, g, h, i) {                              \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << '\t' << (h) << '\t' << (i) << std::endl; \
    std::cerr.flush();                                                  \
  }

#define TSV10_(a, b, c, d, e, f, g, h, i, j) {                          \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << '\t' << (h) << '\t' << (i) << '\t' << (j) << std::endl; \
    std::cerr.flush();                                                  \
  }

#endif  // HELPER_HPP_



/* ************************
 * Macros always included *
 **************************/

/** \brief
 * Print to stderr the element of list, separated by comma.
 *
 * If a NHELPER macro is defined
 * then do nothing.
 *
 * (Useful during development phase.)
 *
 * @param list iterable
 */
#ifdef NHELPER
#  define PRINT_LIST(list) {}
#else
#  define PRINT_LIST(list) {                    \
    std::cerr << '(' << (list).size() << ')';   \
    for (auto x : (list)) {                     \
      std::cerr << ',' << x;                    \
    }                                           \
    std::cerr << std::endl;                     \
    std::cerr.flush();                          \
  }
#endif



/** \brief
 * Print to stderr all parameters separated by tabulation.
 *
 * If a NHELPER macro is defined
 * then do nothing.
 *
 * (Useful during development phase.)
 */
#undef TSV

#ifdef NHELPER
#  define TSV(...)
#else
#  define TSV(...) GET_OVERLOAD_(__VA_ARGS__,                           \
                                 TSV10_, TSV9_, TSV8_, TSV7_, TSV6_, TSV5_, \
                                 TSV4_, TSV3_, TSV2_, TSV1_)(__VA_ARGS__)
#endif



// To avoid cpplint warning
#ifndef HELPER_HPP_
#endif  // HELPER_HPP_
