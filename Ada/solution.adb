with Ada.Environment_Variables;
with Ada.Strings.Unbounded;

with Ada.Text_IO;

use Ada.Text_IO;


procedure Solution is
   FOut : File_Type;
   Line : Ada.Strings.Unbounded.Unbounded_String;
begin
   Create (File => FOut,
           Mode => Out_File,
           Name => Ada.Environment_Variables.Value ("OUTPUT_PATH"));

   while not End_Of_File (Current_Input) loop
      Line := Ada.Strings.Unbounded.To_Unbounded_String
        (Get_Line (Current_Input));
      Put_Line (FOut, Ada.Strings.Unbounded.To_String (Line));
   end loop;

   Close (FOut);
end Solution;
