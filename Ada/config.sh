# Config file for run.sh and foreach.sh (for solution program in Ada)
# See *List of configuration variables* in
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/

# INPUT_PATTERN='data/input/input*.txt'    # pattern of input files
# OUTPUT_PATTERN='data/correct/output*.txt'  # pattern of corresponding correct output files


# VM=''  # virtual machine, to run PROG
# VM_FLAGS=()


ANALYZER='valgrind'  # dynamic analyzer, enabled by -z or -Z options
ANALYZER_FLAGS=('--leak-check=full')


PROG='./solution'  # main program
PROG_FLAGS=()

PROG_SRC='solution.adb'  # source code


# INPUT_HEAD=0   # max number of lines to print, for input file
# RESULT_HEAD=0  # max number of lines to print, for result file
# OUTPUT_HEAD=0  # max number of lines to print, for output file


MAKE_RUN=0  # if 0 then run make step


# DIFF='sdiff'      # side by side differences
# DIFF='colordiff'  # color diff
# DIFF_FLAGS=()


# Predefined others differences viewer, enabled by -v or -V options
## KDiff3
# DIFF_VIEW='kdiff3'
# DIFF_VIEW_FLAGS=()
# DIFF_VIEW_PARAM_RESULT_OUTPUT=''

## Emacs
# DIFF_VIEW='emacs'
# DIFF_VIEW_FLAGS=('--eval')
# DIFF_VIEW_PARAM_RESULT_OUTPUT='(ediff-files "%r" "%o")'

## Vim
# DIFF_VIEW='vim'
# DIFF_VIEW_FLAGS=('-d')
# DIFF_VIEW_PARAM_RESULT_OUTPUT=''
