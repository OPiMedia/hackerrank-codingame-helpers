#!/bin/bash

./copy.sh


DIRS=(Ada AWK Bash C Cpp C-Sharp Haskell Java JavaScript Pascal Perl PHP Python Rust Scala SQL)

DEST='_DOWNLOAD'

mkdir --parents "${DEST}" || exit 1


echo '==================== Overclean ===================='
for DIR in "${DIRS[@]}"; do
    echo "========== Overclean ${DIR}... =========="
    cd "${DIR}" || exit 1
    make keepFileEmptyDirectory forceCleanData overclean || exit 1
    rm --force ./*~ || exit 1
    rm --force ./data/input/*~ || exit 1
    rm --force ./data/correct/*~ || exit 1
    cd .. || exit 1
    echo
done


echo '==================== Build ZIPs ===================='
echo '========== Build ALL_LANGUAGES.zip... =========='
mkdir --parents "${DEST}/ALL_LANGUAGES" || exit 1
cp --recursive --target-directory "${DEST}/ALL_LANGUAGES" "${DIRS[@]}"
cd "${DEST}" || exit 1
zip -mr ALL_LANGUAGES.zip ALL_LANGUAGES || exit 2
cd - || exit 1
echo

for DIR in "${DIRS[@]}"; do
    echo "========== Build ${DIR}.zip... =========="
    zip -r "${DEST}/${DIR}.zip" "${DIR}" || exit 2
    echo
done


echo '=== Build ZIPs: done ==='
