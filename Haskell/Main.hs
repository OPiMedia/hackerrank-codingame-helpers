module Main where

import System.Environment
import System.IO


main :: IO()
main = do
    outputPath <- getEnv "OUTPUT_PATH"
    fOut <- openFile outputPath WriteMode

    contents <- getContents
    hPutStr fOut contents

    hClose fOut
