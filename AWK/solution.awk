#!/usr/bin/awk --file

BEGIN {
    output_path = ENVIRON["OUTPUT_PATH"]
    if (output_path == "") output_path = "/dev/stdout"
}

# Main
{
    print $0 > output_path
}

END {
}
