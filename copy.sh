#!/bin/bash

DIRS=(Ada AWK Bash C Cpp C-Sharp Haskell Java JavaScript Pascal Perl PHP Python Rust Scala SQL)


for FILE in run.sh foreach.sh make_help; do
    echo ==================== Copy foreach.sh ====================
    for DIR in "${DIRS[@]}"; do
        cp --preserve --update "SRC/${FILE}" "${DIR}/" || exit 2
        cmp "SRC/${FILE}" "${DIR}/${FILE}" || echo "\"${DIR}/${FILE}\" is DIFFERENT!"
    done
done

echo '=== Copy: done ==='
