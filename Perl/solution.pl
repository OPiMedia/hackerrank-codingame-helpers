#!/usr/bin/perl

use strict;
use warnings;

use 5.20.1;

STDOUT->autoflush(1);
STDERR->autoflush(1);



open(my $fout, '>', $ENV{'OUTPUT_PATH'});

while (<>) {
        print $fout $_;
}

close $fout;
