/* -*- coding: latin-1 -*- */
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 1024


int main(void) {
  const char* const filename = getenv("OUTPUT_PATH");

  if (filename == NULL) return EXIT_FAILURE;

  char buffer[BUFFER_SIZE];
  FILE* const fout = fopen(filename, "w");

  if (fout == NULL) return EXIT_FAILURE;

  while (fgets(buffer, BUFFER_SIZE, stdin)) {
    fprintf(fout, "%s", buffer);
  }

  fclose(fout);

  return EXIT_SUCCESS;
}
