#!/bin/bash

#
# Usage: make_help [OPTION]... [FILE]...
#
# Prints the list of targets and dependencies,
# with associated description given in comment,
# from one or more Makefile.
# If there is no given FILE, gets "Makefile" by default.
# Use - to specify the standard input.
#
# If the environment variable MAKE_HELP_ARGS is set,
# then use its content as first arguments.
#
# Data option:
#   --delimiter STRING   beginning of a target description (default: "#")
#
# Filtering options:
#   --pattern PATTERN          keep only lines that match PATTERN
#                                 (regular expression like with grep)
#   --invert-pattern PATTERN   select output lines that not match PATTERN
#   -E, --extended-regexp      PATTERNS are extended regular expressions
#   -F, --fixed-strings        PATTERNS are strings
#   -i, --ignore-case          ignore case distinctions
#
# Displaying options:
#   --color                   set --format and --format-no-desc for colored output
#   --format FORMAT           specify the output for a target with a description
#                               (default: "%T: %Y   #%d\n")
#   --format-no-desc FORMAT   specify the output for a target without description
#                               (default: "%T: %y\n")
#   --list                    set --format and --format-no-desc to "%t\n"
#   --no-sort                 do not sort the list
#
# Help options:
#   ---debug-format    prints the current FORMAT values and examples to stderr
#   ---debug-options   prints following command line options to stderr
#   -h, --help         display this help text and exit
#   --version          display version information and exit
#
# FORMAT is a string to specify output line, where each %t, %y and %d
# is replaced by the target, the dependency and the description.
# %T, %Y and %D are the same, except that they add a right padding.
# Moreover, during the setting by --format,
# %f is replaced by the previous FORMAT value (without its ending lines).
#
# Requirements:
# * cat, printf, sed, tr
# * grep for options --pattern and --invert-pattern
#
# Latest version and more explanations on Bitbucket:
# https://bitbucket.org/OPiMedia/make_help/
#
# Apache License 2.0 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
##
# * November 19, 2023: First public version.
# * Started August 5, 2023
#


# Main function
function make_help {  # [param1 ...]
    local -r script_name=$(basename "$0")


    # Prints maximum length of all lines.
    function array_max_length {  # [line ...]
        local -i length=0

        local item
        for item in "$@"; do
	    if [[ $length -lt "${#item}" ]]; then
	        length="${#item}"
	    fi
        done
        echo -n "$length"
    }

    # Prints text_in_red and text to stderr.
    function error_msg {  # text_in_red, text
        echo "$(tput setaf 1)$1$(tput sgr0) $2" 1>&2
    }

    # Prints FORMAT by replacing %t, %y, %d, %T, %Y and %D by corresponding data.
    function evaluate_format {  # target_max_length dependency_max_length description_max_length target dependency description
        local -i -r target_max_length="$1"
        local -i -r dependency_max_length="$2"
        local -i -r description_max_length="$3"
        local -r target="$4"
        local -r dependency="$5"
        local -r description="$6"

        local char
        local substring=''

        local format="$format_complete"

        if [[ -z "$description" ]]; then
            format="$format_no_desc"
        fi

        {
            local -i i
            for ((i = 0; i < ${#format}; ++i)); do
                char="${format:$i:1}"
                if [[ "$char" == '%' ]]; then  # special mark %
                    (( ++i ))
                    char="${format:$i:1}"
                    case "$char" in
                        d)
                            printf '%b%s' "$substring" "$description"
                            substring=''
                            ;;
                        D)
                            printf "%b%-${description_max_length}s" "$substring" "$description"
                            substring=''
                            ;;
                        t)
                            printf '%b%s' "$substring" "$target"
                            substring=''
                            ;;
                        T)
                            printf "%b%-${target_max_length}s" "$substring" "$target"
                            substring=''
                            ;;
                        y)
                            printf '%b%s' "$substring" "$dependency"
                            substring=''
                            ;;
                        Y)
                            printf "%b%-${dependency_max_length}s" "$substring" "$dependency"
                            substring=''
                            ;;
                        '')  # the mark % is not following by other character
                            substring+='%'
                            ;;
                        *)  # the mark % is following by normal character
                            substring+="$char"
                    esac
                else                           # normal character
                    substring+="$char"
                fi
            done
            printf '%b' "$substring"
        } | sed -E 's/[[:space:]]+$//'
    }  # evaluate_format

    # Reads stdin
    # and prints to stdout each line that doesn't match any PATTERN of grep_invert_patterns.
    function filter_by_invert_patterns {
        if [[ "${#grep_invert_patterns[@]}" -eq 0 ]]; then  # no invert PATTERN
            cat
        else                                                # some invert PATTERN
            grep "${grep_options[@]}" --invert-match "${grep_invert_patterns[@]}"
        fi
    }

    # Reads stdin
    # and prints to stdout each line that matches all PATTERN of grep_patterns.
    function filter_by_patterns {
        if [[ "${#grep_patterns[@]}" -eq 0 ]]; then  # no PATTERN
            cat
        else                                         # some PATTERN
            lines=$(cat)

            # Filters
            local pattern
            for pattern in "${patterns[@]}"; do  # take lines that match all patterns
                lines=$(grep "${grep_options[@]}" --regexp "$pattern" <<< "${lines}")
            done

            # Run grep again to color all matches
            grep "${grep_options[@]}" --color "${grep_patterns[@]}" <<< "${lines}"
        fi
    }

    # Prints list of characters.
    function list_characters {  # string
        local -r string="$1"

        local -i i
        for ((i = 0; i < ${#string}; ++i)); do
            if [[ $i -gt 0 ]]; then
                printf ' '
            fi
            char="${string:$i:1}"
            printf '%3s' "$char"
        done
    }

    # Prints list of character codes of string, in decimal.
    function list_ord_decimal {  # string
        local -r string="$1"

        local -i i
        for ((i = 0; i < ${#string}; ++i)); do
            if [[ $i -gt 0 ]]; then
                printf ' '
            fi
            char="${string:$i:1}"
            printf '%3d' \'"$char"
        done
    }

    # Prints list of character codes of string, in hexadecimal.
    function list_ord_hexadecimal {  # string
        local -r string="$1"

        local -i i
        for ((i = 0; i < ${#string}; ++i)); do
            if [[ $i -gt 0 ]]; then
                printf ' '
            fi
            char="${string:$i:1}"
            printf '%3x' \'"$char"
        done
    }

    # Prints "$2", the next argument given.
    # If not available, call print_help_exit "$1".
    function next_argument {  # [argument1 ...]
        if [[ $# -lt 2 ]]; then  # missing argument
            print_help_exit "$1"
        fi
        printf '%s' "$2"
    }

    # Prints "$2", the next argument given, following by an ending character 'E'.
    # The purpose is to protect trailing newlines when this function is used inside $(...),
    # else the evaluation removes them.
    function next_argument_E {  # [argument1 ...]
        if [[ $# -lt 2 ]]; then  # missing argument
            print_help_exit "$1"
        fi
        printf '%sE' "$2"
    }

    # Prints help message (reads from the script itself) and quits.
    # If called without argument,
    # then prints to stdout and exit 0,
    # else prints to stderr and exit 1.
    function print_help_exit {  # [wrong option]
        local -r wrong_option="$1"
        local -i return_code=0

        if [[ $# -ne 0 ]]; then
            return_code=1
            exec 1>&2
            error_msg 'Unknown or wrong option!' "\"$wrong_option\""
        fi

        local -i skip_begin=0

        local line
        while IFS='' read -r line; do  # prints the header comment until the line ##
            if [[ "$line" == '# Usage:'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            if [[ "$line" == '##'* ]]; then break; fi
            line="${line:2}"
            printf '%s\n' "${line//make_help/$script_name}"
        done < "${BASH_SOURCE[0]}"

        echo "Current version: $(print_version)"

        exit $return_code
    }

    # Prints string surrounded by simple quotes '...',
    # and replace each ' character by '"'"'.
    # So the concatenation of all parts is a correct representation of string.
    # The purpose is to overcome
    # the impossibility to escape a simple quote in a string represented inside simple quotes.
    # For example:
    #   print_in_simple_quotes 'ok, no problem', prints 'ok, no problem'
    #   print_in_simple_quotes "I'am difficult", prints 'I'"'"'am difficult'
    function print_in_simple_quotes {  # string
        printf "'%s'" "${1//\'/\'\"\'\"\'}"
    }

    # Prints all arguments on stderr, surrounded by [].
    function print_options {  # [option1 ...]
        {
            echo -n 'Following command line options:'
            while [[ $# -ne 0 ]]; do
                echo -n " [$1]"
                shift
            done;
            echo
        } 1>&2
    }

    # Prints version (reads it from the script itself, just after the first ## line).
    function print_version {
        local -i skip_begin=0
        local version

        local line
        while IFS='' read -r line; do
            if [[ "$line" == '##'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            line="${line:4}"
            if [[ -n "$line" ]]; then
                IFS=':' read -r version line <<< "$line"
                printf '%s\n' "$version"

                break
            fi
        done < "${BASH_SOURCE[0]}"
    }

    # Sort the three arrays targets, dependencies and descriptions.
    function sort_targets_dependencies_descriptions {
        local -i is_swap
        local tmp

        local -i i
        for ((i = ${#targets[@]} - 1; i > 0; --i)); do  # bubble sort
            local -i j
            for ((j = 0; j < i; ++j)); do
                is_swap=1
                if [[ "${targets[$j]}" > "${targets[$((j + 1))]}" ]]; then
                    is_swap=0
                elif [[ "${targets[$j]}" == "${targets[$((j + 1))]}" ]]; then
                    if [[ "${dependencies[$j]}" > "${dependencies[$((j + 1))]}" ]]; then
                        is_swap=0
                    elif [[ "${dependencies[$j]}" == "${dependencies[$((j + 1))]}" ]]; then
                        if [[ "${descriptions[$j]}" > "${descriptions[$((j + 1))]}" ]]; then
                            is_swap=0
                        fi
                    fi
                fi

                if [[ $is_swap -eq 0 ]]; then
                    tmp="${targets[$j]}"
                    targets[$j]="${targets[$((j + 1))]}"
                    targets[$((j + 1))]="$tmp"

                    tmp="${dependencies[$j]}"
                    dependencies[$j]="${dependencies[$((j + 1))]}"
                    dependencies[$((j + 1))]="$tmp"

                    tmp="${descriptions[$j]}"
                    descriptions[$j]="${descriptions[$((j + 1))]}"
                    descriptions[$((j + 1))]="$tmp"
                fi
            done  # for j
        done  # for i
    }  # sort_targets_dependencies_descriptions

    # Prints string without its leading and trailing spaces.
    function trim {  # string
        sed -E --expression 's/^[[:space:]]+//' --expression 's/[[:space:]]+$//' <<< "$1"
    }

    # Prints string without its trailing spaces.
    function trim_right {  # string
        sed -E 's/[[:space:]]+$//' <<< "$1"
    }


    #
    # Reads arguments and set options
    #
    local -a files
    local -a grep_invert_patterns
    local -a grep_options
    local -a grep_patterns
    local -a patterns

    local -i is_sort=0  # 0 is for true, other is for false

    local delimiter='#'
    local format_complete='%T: %Y   #%d\n'
    local format_no_desc='%T: %y\n'

    local -r color_target='\e[95m\e[1m'  # light magenta, bold
    local -r color_desc='\e[93m'  # light yellow
    local -r color_reset='\e[0m'

    local tmp

    while [[ $# -ne 0 ]]; do
        case "$1" in
            --color)
                format_complete="$color_target%T$color_reset: $color_desc%Y$color_reset   #%d\n"
                format_no_desc="$color_target%T$color_reset: $color_desc%y$color_reset\n"
                ;;
            ---debug-format)
                {
                    echo '=== With a description ==='
                    printf 'Current FORMAT: %s\n' "$(print_in_simple_quotes "$format_complete")"
                    printf 'Characters:  %s\n'    "$(list_characters        "$format_complete")"
                    printf 'Hexadecimal: %s\n'    "$(list_ord_hexadecimal   "$format_complete")"
                    printf 'Decimal:     %s\n'    "$(list_ord_decimal       "$format_complete")"
                    echo '--- Two examples ---'
                    evaluate_format 12 9 18 'one_target'   ''          'first description'
                    evaluate_format 12 9 18 'other_target' 'depend_of' 'second description'
                    echo '=== Without description ==='
                    printf 'Current FORMAT: %s\n' "$(print_in_simple_quotes "$format_no_desc")"
                    printf 'Characters:  %s\n'    "$(list_characters        "$format_no_desc")"
                    printf 'Hexadecimal: %s\n'    "$(list_ord_hexadecimal   "$format_no_desc")"
                    printf 'Decimal:     %s\n'    "$(list_ord_decimal       "$format_no_desc")"
                    echo '--- Two examples ---'
                    evaluate_format 12 9 18 'one_target'
                    evaluate_format 12 9 18 'other_target' 'depend_of'
                    echo '=== Examples end ==='
                } 1>&2
                ;;
            ---debug-options)
                print_options "$@"
                ;;
            --delimiter)
                delimiter=$(next_argument "$@") || exit $?
                shift
                ;;
            --format)
                tmp=$(next_argument_E "$@") || exit $?
                format_complete=$(sed -E 's/(\\n)+$//' <<< "$format_complete")  # remove ending lines in current format_complete
                tmp="${tmp//%f/$format_complete}"
                format_complete="${tmp%E}"  # remove guard E used to keep possible ending lines
                shift
                ;;
            --format-no-desc)
                tmp=$(next_argument_E "$@") || exit $?
                format_no_desc=$(sed -E 's/(\\n)+$//' <<< "$format_no_desc")  # remove ending lines in current format_no_desc
                tmp="${tmp//%f/$format_no_desc}"
                format_no_desc="${tmp%E}"  # remove guard E used to keep possible ending lines
                shift
                ;;
            -E|--extended-regexp|-F|--fixed-strings|-i|--ignore-case)
                grep_options+=("$1")
                ;;
            -h|--help)
                print_help_exit
                ;;
            --list)
                format_complete="%t\n"
                format_no_desc="%t\n"
                ;;
            --no-sort)
                is_sort=1
                ;;
            --invert-pattern)
                local pattern

                pattern=$(next_argument_E "$@") || exit $?
                pattern="${pattern%E}"  # remove guard E used to keep possible ending lines
                grep_invert_patterns+=(--regexp "$pattern") || exit $?
                shift
                ;;
            --pattern)
                local pattern

                pattern=$(next_argument_E "$@") || exit $?
                pattern="${pattern%E}"  # remove guard E used to keep possible ending lines
                grep_patterns+=(--regexp "$pattern")
                patterns+=("$pattern")
                shift
                ;;
            --version)
                echo "make_help ($(print_version))"

                return 0
                ;;
            -)  # adds the standard input
                files+=('-')
                ;;
            -*)  # unknown argument
                print_help_exit "$1"
                ;;
            *)  # adds this file
                files+=("$1")
                ;;
        esac
        shift
    done  # [[ $# -ne 0 ]]

    if [[ "${#files[@]}" -eq 0 ]]; then  # adds default file
        files+=(Makefile)
    fi


    #
    # Main
    #
    local -a targets
    local -a dependencies
    local -a descriptions

    # Parse one Makefile readed from stdin.
    function parse_stdin {
        local target
        local dependency
        local description

        local line
        while read -r line; do
            if [[ "$line" =~ ^[^\ #:]+: && ! "$line" =~ ^.(PHONY|SUFFIXES): ]]; then
                # Takes target
                line=$(tr $'\t' ' ' <<< "$line")
                IFS=':' read -r target line <<< "$line"
                targets+=("$target")

                # Takes dependency
                IFS='#' read -r dependency line <<< "$line"
                dependency=$(trim "$dependency")
                dependencies+=("$dependency")
                line="#$line"  # reintroduces '#' before try to find the delimiter pattern

                # Takes description
                description="${line#*"$delimiter"}"
                if [[ "$description" == "$line" ]]; then  # there has no match of "$delimiter"
                    description=''
                fi
                descriptions+=("$(trim_right "$description")")
            fi
        done
    }

    # Parses each file to find targets, dependencies and descriptions
    local file
    for file in "${files[@]}"; do
        if [[ "$file" == '-' ]]; then  # reads from stdin
            parse_stdin
        else                           # reads from "$file"
            if [[ ! -r "$file" ]]; then
                error_msg "File \"$file\" not readable!"

                continue
            fi
            parse_stdin < "$file"
        fi
    done

    if [[ "${#targets[@]}" -eq 0 ]]; then  # no result
        return 0
    fi

    if [[ $is_sort -eq 0 ]]; then
        sort_targets_dependencies_descriptions
    fi

    # Computes max length of targets and dependencies (to align data in a table below)
    local -i target_max_length
    local -i dependency_max_length
    local -i description_max_length

    target_max_length=$(array_max_length "${targets[@]}")
    dependency_max_length=$(array_max_length "${dependencies[@]}")
    description_max_length=$(array_max_length "${descriptions[@]}")

    # Prints result
    local char
    local substring

    local -i i
    for i in "${!targets[@]}"; do
        evaluate_format "$target_max_length" "$dependency_max_length" "$description_max_length" \
                        "${targets[$i]}" "${dependencies[$i]}" "${descriptions[$i]}"
    done | filter_by_invert_patterns | filter_by_patterns
}  # make_help



# shellcheck disable=SC2086  # no double quotes for the environment variable MAKE_HELP_ARGS
make_help $MAKE_HELP_ARGS "$@"

exit 0
