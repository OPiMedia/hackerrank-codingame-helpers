use std::io::prelude::*;


fn main() {
    let filename = std::env::var("OUTPUT_PATH")
        .expect("environment variable OUTPUT_PATH not defined");
    let mut fout = std::fs::File::create(filename)
        .expect("unable to create result file");
    let stdin = std::io::stdin();

    for line in stdin.lock().lines() {
        fout.write_all(line.unwrap().as_bytes())
            .expect("unable to write to result file");
        fout.write_all(b"\n")
            .expect("unable to write eol to result file");
    }
}
