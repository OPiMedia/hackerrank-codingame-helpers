#!/bin/bash

function extract {
    echo '```'
    sed '1,/<<</d;/>>>/,$d' "$1" | sed 's/declare -[ai]g //'
    echo '```'
}



cp SRC/readme_src.md README.md

{
    printf '\n\n## List of configuration variables\n'

    # shellcheck disable=SC2016  # false positive due to backtick symbol
    echo '### Configuration variables for `run.sh`'
    extract 'SRC/run.sh'

    # shellcheck disable=SC2016  # false positive due to backtick symbol
    printf '\n\n### Configuration variables for `foreach.sh`\n'
    extract 'SRC/foreach.sh'
} >> README.md
