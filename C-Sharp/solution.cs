using System;
using System.IO;


class Solution
{
    static void Main(string[] args)
    {
        TextWriter fout = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"));
        string line;

        while ((line = Console.ReadLine()) != null)
        {
            fout.WriteLine(line);
        }

        fout.Flush();
        fout.Close();
    }
}
