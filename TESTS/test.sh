#!/bin/bash

DIRS=(Ada AWK Bash C Cpp C-Sharp Haskell Java JavaScript Pascal Perl PHP Python Rust Scala SQL)


CURRENT_DIR="${PWD}"
TMP_DIR="${CURRENT_DIR}/_tmp"
TMP="${TMP_DIR}/tmp.txt"



# Remove time and Memcheck indications and part of URL from ${TMP}.
# Write to $1.
function clean_tmp_file {
    sed -E 's/^(real|user|sys)\s+[0-9]+\.[0-9]+$/\1 xxx/g' "${TMP}" \
        | sed -E 's/^==[0-9]{4,6}==(.*)$/==xxxxx==\1/g' \
        | sed -E 's|(<file://)'"${TMP_DIR}"'(/[^>]+)>|\1xxx\2|g' \
        | sed -E 's|(<file://)'"${TMP_DIR}"'(/[^>]+)>|\1xxx\2|g' \
              > "$1"
}


# Delete ${TMP_DIR}.
function run_clean {
    echo "########## clean \"${TMP_DIR}\" ##########"
    if [[ -d "${TMP_DIR}" ]]; then
        rm --recursive "${TMP_DIR}"
    fi
}


# Copy necessary file to ${TMP_DIR}.
function run_copy {
    echo '########## copy ##########'

    mkdir --parents "${TMP_DIR}"  || exit 1

    for DIR in "${DIRS[@]}"; do
        cp --recursive "../$DIR" "${TMP_DIR}/" || exit 1
        if [[ ${DIR} == 'SQL' ]]; then
            rm --recursive "${TMP_DIR}/$DIR/data" || exit 1
            cp --recursive "SQL/data/" "${TMP_DIR}/$DIR" || exit 1
            cp "SQL/solution.sql" "${TMP_DIR}/$DIR" || exit 1
        else
            cp --recursive "data/" "${TMP_DIR}/$DIR/" || exit 1
        fi
    done
}


# Test of foreach.sh, written to ${TMP}.
function run_test_foreach_sh {
    echo '########## test foreach.sh ##########'
    echo "########## test foreach.sh ($(date)) ##########" > "${TMP}"

    cd "${TMP_DIR}/Python" || exit 2

    {
        echo '#################### Python: --version ####################'
        ./foreach.sh --version
        echo "Exit status: $?"
        echo '#################### Python: --help ####################'
        ./foreach.sh --help
        echo "Exit status: $?"

        echo '#################### Python: -l ####################'
        ./foreach.sh -l
        echo "Exit status: $?"
        echo '#################### Python: -L ####################'
        ./foreach.sh -L
        echo "Exit status: $?"

        echo '#################### Python: -- ####################'
        ./foreach.sh --
        echo "Exit status: $?"
        echo '#################### Python: -t ####################'
        ./foreach.sh -t
        echo "Exit status: $?"
        echo '#################### Python: -e ####################'
        ./foreach.sh -e
        echo "Exit status: $?"
    } >> "${TMP}" 2>&1


    cd "${TMP_DIR}/C" || exit 2

    {
        echo '#################### C: -m ####################'
        ./foreach.sh -m
        echo "Exit status: $?"
        echo '#################### C: -M ####################'
        ./foreach.sh -M
        echo "Exit status: $?"
    } >> "${TMP}" 2>&1


    for DIR in "${DIRS[@]}"; do
        echo "#################### $DIR ####################" >> "${TMP}"

        cd "${TMP_DIR}/$DIR" || exit 2

        ./foreach.sh >> "${TMP}" 2>&1
        echo "Exit status: $?" >> "${TMP}"

        cd "${CURRENT_DIR}" || exit 2
    done

    cd "${CURRENT_DIR}" || exit 2
}  # end run_test_foreach_sh


# Test of run.sh, written to ${TMP}.
function run_test_run_sh {
    echo '########## test run.sh ##########'
    echo "########## test run.sh ($(date)) ##########" > "${TMP}"

    cd "${TMP_DIR}/Python" || exit 3

    {
        echo '#################### Python: --version ####################'
        ./run.sh --version;
        echo "Exit status: $?"
        echo '#################### Python: --help ####################'
        ./run.sh --help;
        echo "Exit status: $?"

        echo '#################### Python: -- ####################'
        ./run.sh --
        echo "Exit status: $?"
        echo '#################### Python: -t ####################'
        ./run.sh -t
        echo "Exit status: $?"
        echo '#################### Python: -e ####################'
        ./run.sh -e
        echo "Exit status: $?"

        echo '#################### Python: _with_without ####################'
        ./run.sh _with_without
        echo "Exit status: $?"
        echo '#################### Python: -e _with_without ####################'
        ./run.sh -e _with_without
        echo "Exit status: $?"
    } >> "${TMP}" 2>&1


    cd "${TMP_DIR}/C" || exit 3

    {
        echo '#################### C: -m ####################'
        ./run.sh -m
        echo "Exit status: $?"
        echo '#################### C: -z ####################'
        ./run.sh -z
        echo "Exit status: $?"
    } >> "${TMP}" 2>&1


    for DIR in "${DIRS[@]}"; do
        echo "#################### $DIR ####################" >> "${TMP}"

        cd "${TMP_DIR}/$DIR" || exit 3

        {
            ./run.sh >> "${TMP}"
            echo "Exit status: $?"
        } >> "${TMP}" 2>&1

        cd "${CURRENT_DIR}" || exit 3
    done

    cd "${CURRENT_DIR}" || exit 3
}  # end run_test_run_sh



# Main
echo '==================== Test ===================='

run_clean
run_copy


time run_test_run_sh
clean_tmp_file "${CURRENT_DIR}/test_log_run_sh.txt"

# https://github.com/koalaman/shellcheck
{
    echo '#################### shellcheck ####################'
    shellcheck --version | head --lines 2 | tr '\n' ' '
    echo

    echo '#################### shellcheck test.sh ####################'
    shellcheck test.sh

    echo '#################### shellcheck ../SRC/run.sh ####################'
    shellcheck ../SRC/run.sh
} >> "${CURRENT_DIR}/test_log_run_sh.txt" 2>&1


time run_test_foreach_sh
clean_tmp_file "${CURRENT_DIR}/test_log_foreach_sh.txt"

{
    echo '#################### shellcheck ../SRC/foreach.sh ####################'
    shellcheck ../SRC/foreach.sh
} >> "${CURRENT_DIR}/test_log_foreach_sh.txt" 2>&1


read -r -p 'Clean "'"TMP_DIR"'" directory? [Y/n]' ANSWER
if [[ "${ANSWER}" == 'Y' ]]; then
    run_clean
fi

echo '=== Copy: done ==='
exit 0
