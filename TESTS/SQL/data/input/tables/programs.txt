gocd_(Bash) https://bitbucket.org/OPiMedia/gocd-bash/ Bash
assertOpenCL https://bitbucket.org/OPiMedia/assertopencl/ OpenCL
HackerRank_[CodinGame]_/_helpers https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/ Bash
OPiCitations http://www.opimedia.be/OPiCitations/ PHP
Scala-Par-AM https://bitbucket.org/OPiMedia/scala-par-am/ Scala
parallel_sigma_odd_problem https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/ C++
mixed_radix http://www.opimedia.be/mixed-radix/ JavaScript
