CREATE TABLE languages (
    language VARCHAR(100),
    url VARCHAR(100)
);


CREATE TABLE programs (
    program VARCHAR(100),
    url VARCHAR(100),
    language VARCHAR(100)
);
