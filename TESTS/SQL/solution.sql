SELECT program, programs.url, programs.language, languages.url
FROM programs INNER JOIN languages
ON programs.language = languages.language
ORDER BY UPPER(program);
