# Original images

* [lamp.svg](https://www.flaticon.com/free-icon/lamp_2910875)
  ([Pixel perfect](https://www.flaticon.com/authors/pixel-perfect))
* [processing.svg](https://www.flaticon.com/free-icon/processing_2399052)
  ([surang](https://www.flaticon.com/authors/surang))
* [success.svg](https://www.flaticon.com/free-icon/success_2830919)
  ([freepik](https://www.flaticon.com/authors/freepik))
