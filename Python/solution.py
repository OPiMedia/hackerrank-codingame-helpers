#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    """
    Like print() but on stderr by default, with flush.
    Disabled if not __debug__.
    """
    if __debug__:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def logn(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
         flush: bool = True, to_str=repr) -> None:
    """
    Like log() but one param by line and print iterable one element by line.
    Disabled if not __debug__.
    """
    if __debug__:
        import collections.abc  # pylint: disable=import-outside-toplevel

        for param in params:
            if (not isinstance(param, str) and
                    isinstance(param, collections.abc.Iterable)):
                if isinstance(param, collections.abc.Mapping):
                    for key, value in param.items():
                        log(f'{to_str(key)}: {to_str(value)}',
                            sep=sep, end=end, file=file, flush=flush)
                else:
                    for value in param:
                        log(to_str(value), sep=sep, end=end, file=file,
                            flush=flush)
            else:
                log(to_str(param), sep=sep, end=end, file=file, flush=flush)


def main() -> None:
    with open(os.environ['OUTPUT_PATH'], 'w', encoding='latin-1') as fout:
        for line in sys.stdin:
            fout.write(line)


if __name__ == '__main__':
    main()
