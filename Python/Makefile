# Static analysis and cleaning (for Python language) --- December 3, 2023
PROG_SRC = $(sort $(wildcard *.py))
SRC = $(PROG_SRC)


.SUFFIXES:

###########
# Options #
###########
PYTHON      = python3  # https://www.python.org/
PYTHONFLAGS =

PYPY      = pypy3  # https://www.pypy.org/
PYPYFLAGS =


MYPY      = mypy  # http://www.mypy-lang.org/
MYPYFLAGS = # --warn-unused-ignores

PYCODESTYLE      = pycodestyle  # (pep8) https://pypi.org/project/pycodestyle/
PYCODESTYLEFLAGS = --statistics  # --ignore=E501

PYFLAKES      = pyflakes  # https://pypi.org/project/pyflakes/
PYFLAKESFLAGS =

PYLINT      = pylint  # https://www.pylint.org/
PYLINTFLAGS = --jobs $(JOB) --disable=line-too-long,locally-disabled,missing-class-docstring,missing-function-docstring,missing-module-docstring --good-names=a,b,c,d,e,f,g,h,i,j,k,l,m,n,nb,o,ok,p,q,r,s,t,u,v,w,x,x0,x1,y,y0,y1,z

PYRE      = pyre  # https://pyre-check.org/
PYREFLAGS = # --strict

PYTYPE      = pytype  # https://google.github.io/pytype/
PYTYPEFLAGS = --pythonpath $(PWD):$(PYTHONPATH) --keep-going --jobs $(JOB)


DOS2UNIX = dos2unix
ECHO     = echo
GREP     = grep
HEAD     = head
LS       = ls
READ     = read
RM       = rm --force
RMDIR    = rmdir
SHELL    = bash
TEE      = tee
UNIX2DOS = unix2dos

FINISHED      = finished.sh  # https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/
FINISHEDFLAGS =

MAKE_HELP      = ./make_help  # https://bitbucket.org/OPiMedia/make_help/
MAKE_HELPFLAGS = --color



JOB ?= 1  # change this by define new value when start: $ make JOB=3



###
# #
###
.PHONY:	all dos2unix finished unix2dos

all:	lintlog  # first target, runs lintlog target

dos2unix:  # convert data files to Unix end of lines
	$(DOS2UNIX) data/correct/*.txt
	$(DOS2UNIX) data/input/*.txt

finished:
	$(FINISHED) $(FINISHEDFLAGS)

unix2dos:  # convert data files to DOS end of lines
	$(UNIX2DOS) data/correct/*.txt
	$(UNIX2DOS) data/input/*.txt



###################
# Static analysis #
###################
.PHONY: lint lintlog mypy pycodestyle pyflakes pylint pyre pytype

lint:	pycodestyle pyflakes pylint mypy pytype # runs each static analyser

lintlog:	# runs each static analyser and save to lint.log file
	@$(ECHO) 'Lint ('`date`') of solution.py' | $(TEE) lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== pycodestyle '(pep8)' `$(PYCODESTYLE) $(PYCODESTYLEFLAGS) --version` ===== | $(TEE) --append lint.log
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC) 2>&1 | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== pyflakes `$(PYFLAKES) $(PYFLAKESFLAGS) --version` ===== | $(TEE) --append lint.log
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC) 2>&1 | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== `$(PYLINT) $(PYLINTFLAGS) --version` ===== | $(TEE) --append lint.log
	-$(PYLINT) $(PYLINTFLAGS) $(SRC) 2>&1 | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== `$(MYPY) $(MYPYFLAGS) --version` ===== | $(TEE) --append lint.log
	-export MYPYPATH=$(PWD):$(PYTHONPATH); $(MYPY) $(MYPYFLAGS) $(SRC) 2>&1 | $(TEE) --append lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== pytype `$(PYTYPE) $(PYTYPEFLAGS) --version` ===== | $(TEE) --append lint.log
	-$(PYTYPE) $(PYTYPEFLAGS) $(PROG_SRC) 2>&1 | $(TEE) --append lint.log
#	@$(ECHO) | $(TEE) --append lint.log
#	@$(ECHO) ===== Pyre `$(PYRE) $(PYREFLAGS) --version | $(HEAD) --lines 1` ===== | $(TEE) --append lint.log
#	-$(PYRE) $(PYREFLAGS) --noninteractive check 2>&1 | $(GREP) -E -v '[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+ \[PID [0-9]+\] [^ES]' | $(TEE) --append lint.log


mypy:	# runs static analyser mypy
	@$(ECHO) ===== `$(MYPY) $(MYPYFLAGS) --version` =====
	-export MYPYPATH=$(PWD):$(PWD)/tests:$(PYTHONPATH); $(MYPY) $(MYPYFLAGS) $(SRC)
	@$(ECHO)

pycodestyle:	# runs static analyser pycodestyle
	@$(ECHO) ===== pycodestyle '(pep8)' `$(PYCODESTYLE) $(PYCODESTYLEFLAGS) --version` =====
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC)
	@$(ECHO)

pyflakes:	# runs static analyser Pyflakes
	@$(ECHO) ===== pyflakes `$(PYFLAKES) $(PYFLAKESFLAGS) --version` =====
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC)
	@$(ECHO)

pylint:	# runs static analyser Pylint
	@$(ECHO) ===== `$(PYLINT) $(PYLINTFLAGS) --version` =====
	-$(PYLINT) $(PYLINTFLAGS) -f colorized $(SRC)
	@$(ECHO)

pyre:	.pyre_configuration  # runs static analyser Pyre
	@$(ECHO) ===== Pyre `$(PYRE) $(PYREFLAGS) --version | $(HEAD) --lines 1` =====
	-$(PYRE) $(PYREFLAGS) check
	@$(ECHO)

.pyre_configuration:
	$(ECHO) '\n' | $(PYRE) init

pytype:	# runs static analyser pytype
	@$(ECHO) ===== pytype `$(PYTYPE) $(PYTYPEFLAGS) --version` =====
	-$(PYTYPE) $(PYTYPEFLAGS) $(PROG_SRC)
	@$(ECHO)



#########
# Clean #
#########
.PHONY:	clean cleanData cleanLint cleanResult distclean forceCleanData keepFileEmptyDirectory overclean

clean:	cleanResult
	$(RM) --recursive .mypy_cache
	$(RM) .pyre_configuration
	$(RM) --recursive .pyre
	$(RM) --recursive .pytype
	@if [ -z $(KEEPFILEEMPTYDIRECTORY) ]; then $(RM) data/correct/PUT_CORRECT_OUTPUT_FILES_HERE data/input/PUT_INPUT_FILES_HERE; fi

cleanData:	# asks confirmation and then deletes data files
	-@$(LS) -l data/correct/*.txt data/input/*.txt 2>/dev/null
	@if [ -z $(FORCECLEANDATA) ]; then $(READ) -p 'Delete "data" directory? [Y/n]' answer; if [ "$$answer" != 'Y' ]; then exit 1; fi; fi
	-$(RM) data/correct/*.txt
	-$(RM) data/input/*.txt

cleanLint:
	$(RM) lint.log

cleanResult:
	-$(RM) result/*.txt
	-$(RMDIR) result

distclean:	clean

forceCleanData:	# configures cleanData target to not ask confirmation
	$(eval FORCECLEANDATA = true)

keepFileEmptyDirectory:	# configures clean target to keep special files in data directories
	$(eval KEEPFILEEMPTYDIRECTORY = true)

overclean:	distclean cleanLint cleanData



########
# Help #
########
.PHONY:	h help

h:  # display only targets with description
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc ''

help:   # display all targets
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc '  %f\n'
