# HackerRank [CodinGame…] / helpers
Little scripts (to compile, static analyze, **run, and compare results**)
to help in **solving problems of HackerRank** website (or **CodinGame…**),
in several programming languages
(Ada, AWK, Bash, C, C++, C#, Haskell, Java, JavaScript, Pascal, Perl, PHP, Python, Rust, Scala and SQL).

You can easily adapt these scripts for **other programming languages**.

Directories for each language:
[Ada](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Ada/),
[AWK](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/AWK/),
[Bash](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Bash/),
[C](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/C/),
[C++](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Cpp/),
[C#](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/C-Sharp/),
[Haskell](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Haskell/),
[Java](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Java/),
[JavaScript (Node.js)](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/JavaScript/),
[Pascal](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Pascal/),
[Perl](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Perl/),
[PHP](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/PHP/),
[Python](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/),
[Rust](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Rust/),
[Scala](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Scala/)
and
[SQL](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/SQL/).

Generic sources:
[SRC/](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/SRC/).

Scripts for each language can be downloaded in a ZIP file from
[Downloads](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/downloads/)
section.

Each of these language directories or ZIP file contains at least 6 similar files.
Here for the Python language:

* [`solution.py`](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/solution.py):
  your solution program.
  [The only file to modify if you take an already handled language, with its default configuration.]
* [`Makefile`](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/Makefile):
  directives for `make` command (compile, static analysis, cleaning).
* [`run.sh`](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/run.sh):
  main Bash script to run and compare on one input file.
  [Identical for each language.]
* [`foreach.sh`](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/foreach.sh):
  Bash script to run and compare on several input files, in fact calls `run.sh`.
  [Identical for each language.]
* [`config.sh`](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/config.sh):
  Bash variables to specialize generic scripts `run.sh` and `foreach.sh`.
* [`.gitignore`](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/.gitignore):
  A default Git configuration file (to help quick use of Git).
* [`data/`](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/Python/data/):
  Directory where to put input and correct output files.
  [Put `input*.txt` files in `data/input/`, and correct `output*.txt` files in `data/correct/`.]

`run.sh` and `foreach.sh` are exactly the same in all directories.
Particularities for each language are specified in `Makefile` and `config.sh`.
See below *List of configuration variables*.



## Usage
### `run.sh`: run and compare on one input file
Put input data in a file `data/input/input.txt`
and **correct** output data in a file `data/correct/output.txt`.
(See below for SQL.)
Write your code in solution program
(for example for Python in the file `solution.py`),
and do

```bash
$ ./run.sh
```

That compiles (for languages that must be, by `make` command),
runs solution program on input file (a result file is written),
and compares result file with correct output file.

```
                             +-----------+
                             | your code |
                             +-----------+
                               | compile (or simply get)
                               v
+----------------------+     +------------------+     +-------------------+
| data/input/input.txt | --> | solution program | --> | result/result.txt |
+----------------------+     +------------------+     +-------------------+ |    +-----+
                                                                            |--> | cmp |
                                                +-------------------------+ |    +-----+
                                                | data/correct/output.txt |
                                                +-------------------------+
```

```bash
$ ./run.sh 01
```

does the same with a given common part `01`.
Files used are `data/input/input01.txt` and `data/correct/output01.txt`,
and solution program write to `result/result01.txt`.


```bash
$ ./run.sh --help
```

display help message with options.


### Default solution programs
For each programming language a default solution program is given:
`solution.adb`,
`solution.awk`,
`solution.sh`,
`solution.c`,
`solution.cpp`,
`solution.cs`,
`Main.hs`,
`Solution.java`,
`solution.js`,
`solution.pas`,
`solution.pl`,
`solution.php`,
`solution.py`,
`solution.rs`,
`Solution.scala`
or
`solution.sql`.
Each of them (except for SQL) simply copy content of standard input to result file.


### Solution program writes to result file
If solution program itself writes to a file,
it have write to a file specified by environment variable `OUTPUT_PATH`.
In general HackerRank problems require that.

Else it have to write to the standard output.
(And the source code must not contain any string `"OUTPUT_PATH"`.)
In this case the `run.sh` script redirect this standard output to the result file.


### `foreach.sh`: run on several input files
```bash
$ ./foreach.sh
```

runs `run.sh` on each COMMON part such that there exist a `data/input/inputCOMMON.txt` file,
and displays a summary.


```bash
$ ./foreach.sh 02 '' 42
```

runs `run.sh` successively on `input02.txt`, `input.txt` and `input42.txt`.


```bash
$ ./foreach.sh --help
```

display help message with options.


### Static analysis
For some languages the given `Makefile` can performs static analysis of your code:

```bash
$ make lint
```

To see which programs static analyzers to install on your system,
see in each `Makefile`.

Displays all targets:

```bash
$ make help
```


### End of line conversion between DOS and Unix
To convert all files `data/correct/*.txt` and `data/input/*.txt` from DOS end of line to Unix end of line.

```bash
$ make dos2unix
```


## Specific case of SQL
Instead to put input data in a file `data/input/input.txt`,
put in this file the association table name with the corresponding file that contains data.
For example, to import data from `data/input/tables/data.txt` to the table `examples`:

```
examples data/input/tables/data.txt
```

You have to put in the file `data/tables.sql` the right SQL code to create your tables.

By default this is [SQLite](https://www.sqlite.org/) that is used.
Database files are created in the `result/dbs/` directory.


## Links to challenge websites
* [Advent of Code](https://adventofcode.com/):
  my [solutions](https://bitbucket.org/OPiMedia/advent-of-code/)
* [CodeSignal](https://codesignal.com/):
  my profile [opimedia](https://app.codesignal.com/profile/opimedia)
* [CodinGame](https://www.codingame.com/):
  my profile [OPi](https://www.codingame.com/profile/3b9f76c06bfe94afb9608048e12411ab3857041)
* [HackerRank](https://www.hackerrank.com/):
  my profile [OPiMedia](https://www.hackerrank.com/OPiMedia)
* [ProjectEuler](https://projecteuler.net/):
  my profile [OPi](https://projecteuler.net/progress=OPi)
* …

* [*Competitive programming*](https://en.wikipedia.org/wiki/Competitive_programming) (Wikipedia)


## Links to languages references
* [**Programming languages**](http://www.opimedia.be/DS/languages/)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## Support me
This program is a **free software** (GPL license).
It is **completely free** (like "free speech" *and like "free beer").
However you can **support me** financially by donating.

Click to this link
[![Donate](http://www.opimedia.be/_png/donate--76x47-t.png)](http://www.opimedia.be/donate/)
**Thank you!**



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2020-2023, 2025 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.


## Changes
* January 7, 2025: Corrected typo.

* December 3, 2023:

     - Added `DIFF_VIEW_PARAM_RESULT_OUTPUT` configuration variable (to start Emacs Ediff).
     - Added predefined configurations to use Emacs or Vim as differences viewer.
     - Replaced bad use of `[*]` by `[@]`.
     - Added use of `finished.sh` in `Makefile`.
     - Added use of `make_help` in `Makefile`.
     - Cleaning `Makefile`.

* December 2, 2023: Few cleaning.
* November 30, 2023: Preparing to use `make_help` in each `Makefile`.

* October 29, 2023:

    - Added AWK version.
    - Little cleaning.

* December 21, 2022: Added `help` target in each `Makefile`.
* December 4, 2022: Python: Disabled Pyre static analyzer in `Makefile`.
* December 8, 2021: Corrected sort and bug about `IFS` expansion.
* November 14, 2021: Cleaned `IFS`.
* October 29, 2021: Cleaned Bash style.

* January 1st, 2021:
    - Added * substitution by COMMON in executed command by `run.sh`.
    - Added SQL version, with use of SQLite.

* December 31, 2020: Added end of line conversion between DOS and Unix in `Makefile`.

* December 30, 2020:

    - Python: Added Pyre static analyzer in `Makefile`.
    - Python: Added `log()` and `logn()` helpers functions.

* December 19, 2020: Python: Updated Pylint static analyzer option in `Makefile`.
* December 6, 2020: Replaced default value 1 of *_HEAD by 3.
* December 5, 2020:

    - `foreach.sh`: Corrected common identification when wildcard finishes input pattern.
    - Replaced special characters %s in patterns by the single *.
    - Replaced wrong postfix name by common.

* December 2, 2020: Ada: Adapted style checking in `Makefile`.
* October 16, 2020: JavaScript: Corrected reading in `solution.js`.
* October 12, 2020:

    - Corrected `.gitignore` for Ada and Haskell versions,
      and added missing solution source files.
    - Customized warning flags of Ada compilation.

* September 25, 2020: Disabled valgrind in `config.sh` of C# version.
* September 24, 2020: Added C#, Pascal and Perl versions.
* September 23, 2020: JavaScript: Replaced 2 spaces indentations by 4 spaces.
* September 20, 2020:

    - `foreach.sh`: Added `-s` option.
    - `foreach.sh`: Replaced magenta color of title by yellow.

* September 16, 2020:

    - `config.sh`: Added predefined `INPUT_PATTERN` and `OUTPUT_PATTERN`.
    - `foreach.sh`: Added `-z` and `-Z` options.
    - C++: Replaced inclusion of `"bits/stdc++.h"` by specific headers.
    - C++: Updated c++14 to c++17 in `Makefile`.
    - Java: Removed 1.8 target version in `Makefile`.
    - Python: Added missing return type of main function.
    - Python: Updated Pylint static analyzer option in `Makefile`.

* September 11, 2020: Enable assertions in `config.sh` for Java.
* September 8, 2020: Split `data/` directory in `data/input` and `data/correct` (the goal is to use same hierarchy as the future other tool outcmp).
* September 4, 2020: Added (commented) option to Infer static analyzer for C.
* September 3, 2020:

    - Added `"data/PUT_INPUT_OUTPUT_FILES_HERE"` cleaning in each `Makefile`.
    - Added use of mathematical library in C and C++.
    - Removed splint C static analyzer (because lot of parse errors).
    - Adapted warnings of sparse C static analyzer.

* August 24, 2020:

    - Added PHP version.
    - Added list of configuration variables in README.
    - Added `*_RUN` configuration variables to enable/disable each step in `run.sh`.

* August 19, 2020.
* Started August 9, 2020.



![HackerRank/helpers](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Aug/18/2045762036-2-hackerrank-codingame-helpers-logo_avatar.png)

([References](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/src/master/_img/_src/_original/)
of images used to create logo)

#HackerRankHelpers

`./run.sh _different`  
![run.sh screenshot](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/raw/master/_img/screenshots/run-sh.png)

Summary part of `./foreach.sh`  
![foreach.sh summary screenshot](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/raw/master/_img/screenshots/foreach-sh-summary.png)



## Other personal Bash free softwares
* [etoolbox](https://bitbucket.org/OPiMedia/etoolbox/):
  Various scripts.
* [gocd (Bash)](https://bitbucket.org/OPiMedia/gocd-bash/):
  Change working directory from a list of association `NAME: DIRECTORY`, with autocompletion.
* [proc_deleted (Bash)](https://bitbucket.org/OPiMedia/proc_deleted-bash/):
  List processes that have some deleted files, and allow to kill them.
* [vNu-scripts](https://bitbucket.org/OPiMedia/vnu-scripts/):
  Shell scripts to run *the Nu Html Checker (v.Nu)* to valid (X)HTML files by identifying if each file is a (X)HTML file and which version of (X)HTML.


## List of configuration variables
### Configuration variables for `run.sh`
```
INPUT_PATTERN='data/input/input*.txt'    # pattern of input files
OUTPUT_PATTERN='data/correct/output*.txt'  # pattern of corresponding correct output files

TIME='time'  # virtual machine, to run PROG (internal Bash command)
TIME_FLAGS=('-p')

ANALYZER=''  # dynamic analyzer, enabled by -z option
ANALYZER_FLAGS=()

ANALYZER_RUN=1  # if 0 then run dynamic analyzer

VM=''  # virtual machine, to run PROG
VM_FLAGS=()

PROG='./solution.py'  # main program
PROG_FLAGS=()

PROG_SRC='solution.py'  # source code

MAKE='make'  # command use to compile
MAKE_FLAGS=()

RESULT_PATTERN='result/result*.txt'  # pattern of result files

CMP='cmp'  # command used to compare
CMP_FLAGS=()

CMP_LAST_EMPTY_LINE=1  # if 0 then compare also the eventual last empty line

DIFF='diff'  # command display difference when different
DIFF_FLAGS=()

DIFF_VIEW='meld'    # differences viewer, enabled by -v option
DIFF_VIEW_FLAGS=('--newtab')
DIFF_VIEW_PARAM_RESULT_OUTPUT=''  # if empty then adds result and output files, else only adds DIFF_VIEW_PARAM_RESULT_OUTPUT by replacing "%r" and "%o" by these files

DIFF_VIEW_PRINT=0  # if 0 then print command to launch DIFF_VIEW when different
DIFF_VIEW_RUN=1  # if 0 then run difference viewer when result and output are different

# Set '' to print all content, or 0 to no print
INPUT_HEAD=3   # max number of lines to print, for input file
RESULT_HEAD=3  # max number of lines to print, for result file
OUTPUT_HEAD=3  # max number of lines to print, for output file

MAKE_RUN=1    # if 0 then run step 0) building by make
INPUT_RUN=0   # if 0 then run step 1) display of input file
PROG_RUN=0    # if 0 then run step 2) run of solution program
CMP_RUN=0     # if 0 then run step 3) compare result with correct output files
RESULT_RUN=0  # if 0 then run step 4) display of result file
OUTPUT_RUN=0  # if 0 then run step 5) display of correct output file


E_NORMAL='\e[0m'
E_MAGENTA='\e[95m'
E_BG_RED='\e[101m'
```


### Configuration variables for `foreach.sh`
```
INPUT_PATTERN='data/input/input*.txt'    # pattern of input files
OUTPUT_PATTERN='data/correct/output*.txt'  # pattern of corresponding correct output files

RUN='run.sh'  # script executed for each COMMON
RUN_FLAGS=()

if [[ -f "./${RUN}" ]]; then
    RUN="./${RUN}"
fi

RESULT_PATTERN='result/result*.txt'   # pattern of result files
RESULT_LOG_PATTERN='result/foreach_log*.txt'  # pattern of result log files

DIFF_VIEW='meld'  # differences viewer, enabled by -v or -V options
DIFF_VIEW_FLAGS=('--newtab')
DIFF_VIEW_PARAM_RESULT_OUTPUT=''  # if empty then adds result and output files, else only adds DIFF_VIEW_PARAM_RESULT_OUTPUT by replacing "%r" and "%o" by these files

DIFF_VIEW_PRINT=0  # if 0 then print command to launch DIFF_VIEW when different

# Set '' to print all content, or 0 to no print
RESULT_HEAD=3  # max number of lines to print, for result file
OUTPUT_HEAD=3  # max number of lines to print, for output file

EMPTY_COMMON='__empty_common__'  # special value to deal with empty key '' in associative table

DIFF_VIEW_RUN=1       # if 0 then run difference viewer when different files
DIFF_VIEW_RUN_ONCE=1  # if 0 then run difference viewer only on first different files

ONLY_PRINT_LIST=1      # if 0 then only print list, don't run anything
ONLY_PRINT_LIST_URL=1  # if 0 then print URLs instead filenames when print list

STOP_AFTER_RUN_FAIL=1  # if 0 then stop after the first "run.sh" execution that fails


E_NORMAL='\e[0m'

E_GREEN='\e[92m'
E_YELLOW='\e[93m'

E_BG_BLUE='\e[44m'
E_BG_RED='\e[101m'
```
