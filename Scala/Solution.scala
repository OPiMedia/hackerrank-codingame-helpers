import java.io.PrintWriter


object Solution {

  def main(args: Array[String]): Unit = {
    val printWriter = new PrintWriter(sys.env("OUTPUT_PATH"))

    for (line <- io.Source.stdin.getLines()) {
      printWriter.println(line)
    }

    printWriter.close()
  }
}
