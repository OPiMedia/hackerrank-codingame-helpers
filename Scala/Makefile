# Build, static analysis and cleaning (for Scala language) --- December 3, 2023

PROGS = Solution.class

ALL_SRCS = $(sort $(wildcard *.scala))



.SUFFIXES:
.SUFFIXES:	.class .scala

###########
# Options #
###########
DOS2UNIX = dos2unix
ECHO     = echo
HEAD     = head
LS       = ls
READ     = read
RM       = rm --force
RMDIR    = rmdir
SHELL    = bash
TEE      = tee
UNIX2DOS = unix2dos

FINISHED      = finished.sh  # https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/
FINISHEDFLAGS =

MAKE_HELP      = ./make_help  # https://bitbucket.org/OPiMedia/make_help/
MAKE_HELPFLAGS = --color


SCALAC      = scalac  # https://www.scala-lang.org/
SCALACFLAGS = -deprecation -explaintypes -feature -unchecked \
	-Xlint:doc-detached -Xlint:nullary-unit \
	-Ywarn-dead-code
# https://docs.scala-lang.org/overviews/compiler-options/


SCALASTYLE      = scalastyle  # http://www.scalastyle.org/
SCALASTYLEFLAGS = -c config/scalastyle_config__cleaned.xml



###
# #
###
.PHONY: all dos2unix finished unix2dos

all:	$(PROGS)  # first target, compiles each program

dos2unix:  # convert data files to Unix end of lines
	$(DOS2UNIX) data/correct/*.txt
	$(DOS2UNIX) data/input/*.txt

finished:
	$(FINISHED) $(FINISHEDFLAGS)

unix2dos:  # convert data files to DOS end of lines
	$(UNIX2DOS) data/correct/*.txt
	$(UNIX2DOS) data/input/*.txt



#########
# Rules #
#########
.PRECIOUS:	%.class

%.class:	%.scala
	$(SCALAC) $(SCALACFLAGS) $^



###################
# Static analysis #
###################
.PHONY:	 scalastyle lint lintlog

scalastyle:	# runs static analyser Scalastyle
	@$(ECHO) ===== `$(SCALASTYLE) $(SCALASTYLEFLAGS) | $(HEAD) --lines 1` =====
	-$(SCALASTYLE) $(SCALASTYLEFLAGS) $(ALL_SRCS)
	@$(ECHO)


lint:	scalastyle  # runs each static analyser

lintlog:	alll  # runs each static analyser and save to lint.log file
	@$(ECHO) 'Lint ('`date`') of Solution' | $(TEE) lint.log
	@$(ECHO) | $(TEE) --append lint.log
	@$(ECHO) ===== `$(SCALASTYLE) $(SCALASTYLEFLAGS) | $(HEAD) --lines 1` ===== | $(TEE) --append lint.log
	-$(SCALASTYLE) $(SCALASTYLEFLAGS) $(ALL_SRCS) 2>&1 | $(TEE) --append lint.log



#########
# Clean #
#########
.PHONY:	clean cleanData cleanLint cleanResult distclean forceCleanData keepFileEmptyDirectory overclean

clean:	cleanResult
	$(RM) *.class
	@if [ -z $(KEEPFILEEMPTYDIRECTORY) ]; then $(RM) data/correct/PUT_CORRECT_OUTPUT_FILES_HERE data/input/PUT_INPUT_FILES_HERE; fi

cleanData:	# asks confirmation and then deletes data files
	-@$(LS) -l data/correct/*.txt data/input/*.txt 2>/dev/null
	@if [ -z $(FORCECLEANDATA) ]; then $(READ) -p 'Delete "data" directory? [Y/n]' answer; if [ "$$answer" != 'Y' ]; then exit 1; fi; fi
	-$(RM) data/correct/*.txt
	-$(RM) data/input/*.txt

cleanLint:
	$(RM) lint.log

cleanResult:
	-$(RM) result/*.txt
	-$(RMDIR) result

distclean:	clean

forceCleanData:	# configures cleanData target to not ask confirmation
	$(eval FORCECLEANDATA = true)

keepFileEmptyDirectory:	# configures clean target to keep special files in data directories
	$(eval KEEPFILEEMPTYDIRECTORY = true)

overclean:	distclean cleanLint cleanData



########
# Help #
########
.PHONY:	h help

h:  # display only targets with description
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc ''

help:   # display all targets
	@$(ECHO) 'Targets:'
	@$(MAKE_HELP) $(MAKE_HELPFLAGS) --format '  %f\n' --format-no-desc '  %f\n'
