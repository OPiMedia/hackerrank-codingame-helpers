import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class Solution {

    public static void main(final String[] args) throws IOException {
        final BufferedWriter bufferedWriter
            = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        final Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            final String line = scanner.nextLine();

            bufferedWriter.write(line);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
        scanner.close();
    }

}
