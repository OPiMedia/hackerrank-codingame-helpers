<?php

$filename = getenv("OUTPUT_PATH");
if (!$filename) exit(1);

$fout = fopen($filename, "w");
if (!$fout) exit(2);

$stdin = fopen("php://stdin", "r");
if (!$stdin) exit(3);

while (!empty($line = fgets($stdin))) {
    $line = rtrim($line, "\n");

    fwrite($fout, $line . "\n");
}

fclose($stdin);
fclose($fout);
